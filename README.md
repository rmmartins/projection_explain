# Projection Explain

Almighty explainer of projections.

### Notes for Windows Users

* There are compatibility problems between some CUDA toolkit and Microsoft Visual Studio versions, so check [here](http://docs.nvidia.com/cuda/cuda-getting-started-guide-for-microsoft-windows/index.html#system-requirements) before you start.

    * Make sure you have a version of MSVS that supports building x86_32 applications; I'm not sure we can succesfully build on x86_64 right now.

* It's easy to find libraries/headers on Linux because they're (almost) always in the default paths, such as /usr or /usr/local. On Windows, however, they could be anywhere, which makes it harder for CMAKE to find them. So, when installing dependencies, follow these instructions (restart CMAKE-GUI if you change any environment variable while it's open):

    1. Put all .dll's on C:\Windows\System32 (on 32-bit system) or SysWOW64 (on 64-bit system).
    2. Put all .lib's in a folder and point to it with the $LIB environment variable.
    3. Put all .h's in a folder and point to it with the $INCLUDE environment variable.

* If you're on a 64-bit system, you should build **projection_explain** as a 32-bit application. The reason is that many of the dependencies only offer 32-bit pre-compiled binaries and some, such as "triangle", won't even build correctly on Windows 64-bit. There's no downside to building **projection_explain** in 32-bit.

### Dependencies
** Note: you might already have some of the dependencies if you already built CUBu. **

1. [CUBu](https://bitbucket.org/rmmartins/cubu)
    * All platforms: download the sources and follow the instructions on README.md to build and install.
        * Open CMakeLists.txt and point the CUBU variable to the base folder.

2. [Triangle](http://www.cs.cmu.edu/~quake/triangle.html) -- Last tested version: 1.6 (10/08/14)
    * All platforms: Download the sources on the site and extract, but don't build it. We'll build it together with our own code.
        * Open CMakeLists.txt and point the TRIANGLE variable to the base folder.

3. [ANN](http://www.cs.umd.edu/~mount/ANN/) -- Last tested version: 1.1.2 (10/08/14)
    * Win32: There are pre-compiled binaries on the site.
    * Unix: You can probably find it in your package system (ex.: libann-dev). If not, download the sources and use "make linux-g++" to build.

4. [FreeGLUT](http://freeglut.sourceforge.net/) -- Last tested version: 2.8.1 (10/08/2014)
    * Win32: There are pre-compiled binaries [here](http://www.transmissionzero.co.uk/software/freeglut-devel/).
    * Unix: just get it from your packaging system (ex.: freeglut3-dev).

1. [GLUI](http://glui.sourceforge.net/) -- Last tested version: 2.35 (10/08/2014)
    * Win32: Download the sources and build the (ancient) solution found in src/msvc.
        * It depends on (Free)GLUT so add $(INCLUDE) to the project's include dirs.
        * Change the Target Name (on Properties/General) to "glui32" and the Output File (under Properties/Librarian) to "Debug\glui32.lib".
        * This will not generate a DLL. Install glui32.lib and GL/glui.h as defined previously.
    * Unix: just get it from your packaging system (ex.: libglui-dev). On Ubuntu it has been removed from recent repos, but you can find it [here](http://packages.ubuntu.com/raring/libglui-dev), for example.

1. [FreeImage](http://freeimage.sourceforge.net/) -- Last tested version: 3.16.0 (10/08/2014)
    * Win32: There are pre-compiled binaries on the site.
    * Unix: just get it from your packaging system (ex.: libfreeimage-dev).

1. [GLEW](http://glew.sourceforge.net/) -- Last tested version: 1.10.0 (10/08/14)
    * Win32: There are pre-compiled binaries on the site. Install the "glew32" (not "s") lib (together with the DLL).
    * Unix: just get it from your packaging system (ex.: libglew-dev).

### How to Build

1. The preferred way is to build with CMAKE. For example:
    * Win32: run CMAKE-GUI, point to the source folder, create a new "build_win32" folder for the binaries, configure and generate. This will create a VS solution (ProjectionExplain.sln) that you can (hopefully) build without problem. Building the INSTALL project will put the executable in the right place (root of the solution).
    * Unix: mkdir -p build && cd build && cmake .. && make install

2. If you don't want to / can't use CMAKE, try Makefile.old at your own risk. A simple "make" should work. If it doesn't, check Makefile.old to see if everything is correct. Some notes:
    * ANN code is expected to be in "../Geometry/tsANN" (or change the Makefile).
    * Triangle code is expected to be in "../Geometry/Triangle" (or change the Makefile)

### Running

Run with "./projwiz -f data/segmentation lamp -d" to see the example.