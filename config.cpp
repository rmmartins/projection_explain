#include "include/config.h"

// UI settings
int show_brush = 0;

// Cloud drawing
int 		show_particles 	= 0;
float		points_alpha		= 0.5;
float   cloud_color[] 	= {0.0f, 0.0f, 0.0f};

// General settings
float 	shepard_averaging 			= 5;			// alpha
float 	point_influence_radius 	= 20;			// beta

// Selection
int selected_point_id = -1;

float aggregate_error_range = 0.42f;
