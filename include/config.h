extern float 	shepard_averaging;
extern int 		scale_x;
extern int 		scale_y;
extern int 		scale_w;
extern int 		scale_h;
extern int		show_brush;
extern int		current_cmap;
extern float	point_influence_radius;
extern int		show_maptype;
extern int		show_particles;
extern float	points_alpha;
extern float  cloud_color[3];
extern int		selected_point_id;
extern float	 aggregate_error_range;
