#pragma once

//Simple wrapper for hash_map


#if  ( (__GNUC__ > 3) && (__GNUC_MINOR__ >= 3) ) || defined(PLATFORM_WIN)
#ifdef PLATFORM_WIN
#include <unordered_map>
#include <unordered_set>
#else
#include <tr1/unordered_set>
#include <tr1/unordered_map>
#endif
#define hash_set std::tr1::unordered_set
#define hash_multiset std::tr1::unordered_multiset
#define hash_map std::tr1::unordered_map
#define hash_multimap std::tr1::unordered_multimap
#else
#include <hash_set.h>
#include <hash_map.h>
#endif

