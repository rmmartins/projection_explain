#pragma once

#include "include/glwrapper.h"

#include <vector>

#define CMAP_RAINBOW 0


struct Point2d;

namespace projwiz {
	void float2rgb(float value,float& r,float& g,float& b,bool color_or_grayscale = true);
}


void glutDrawString(const char* s);
void glVertex2f(const Point2d&);
void drawSplat(const Point2d& p,float rad);		//Draw a splat (current texture) on a radius of 'rad' pixels centered at 'c'
void setTexture(GLuint tex_id, int tex_interp);
void drawCircle(const Point2d& c,float rad);
int checkLoadColormap(int cmap);
std::vector<std::string> getColormaps();
