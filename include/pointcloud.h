#pragma once

#ifdef PLATFORM_WIN
#define _USE_MATH_DEFINES
#include <math.h>
#endif

#include <vector>
#include "ANN/ANN.h"
#include "include/hashwrap.h"
#include "include/orderedmatrix.h"
#include "include/point2d.h"
#include "grouping.h"

typedef ANNkd_tree ANNtree;		//type of ANN tree to use; can be ANNkd_tree or ANNbd_tree

class  FullMatrix;
class  SparseMatrix;
class  SortedErrorMatrix;
class  Grouping;


struct Color					//General-purpose color
{
	float r,g,b;
	Color() { r=g=b=1; }
	Color(float r_,float g_,float b_): r(r_),g(g_),b(b_) {}
};


struct Triangle					//Triangle defined by 3 point-ids in a given cloud
{
		Triangle() {}
		Triangle(int a,int b,int c) { idx[0]=a; idx[1]=b; idx[2]=c; }
	int operator()(int i) const { return idx[i]; }

	int idx[3];
};


typedef std::vector<Point2d> PointSet;

//----------------------------------------------------------------



class PointCloud							//2D point cloud, with many related helpers (knn, DT, FT, Delaunay, ...)
{
public:

struct Edge {								//An edge formed by 2 points in the cloud. The edge always belongs to a point (its 1st point), not stored in the edge
				Edge(): pid(0),angle(0) {}
				Edge(int pid_,float angle_):pid(pid_),angle(angle_) {}
		float	deg() const { return 360*angle/2/(float)M_PI; }
				int pid;					//The 2nd edge point
				float angle;				//Angle of the edge [0..2*M_PI] with +x axis
			};

class EdgeMatrix : public OrderedMatrix<Edge>										//For each point in the cloud, lists all its edges (to other NEAR points), sorted counterclockwise
{
public:
						EdgeMatrix(int nrows): OrderedMatrix<Edge>(nrows) {}
};

typedef hash_set<int>	TrisOfPoint;												//Idxs of all triangles that contain a given point in the cloud
typedef FullMatrix		DistMatrix;													//Symmetric distance matrix between all points in the cloud


						PointCloud(int fboSize);
					   ~PointCloud();
bool					loadPex(const char* filebase,const char* projname,bool load_nd);	//Load a set of files (nD data, 2D projection, proj-error) from PEx
void					makeKDT();
void					makeKDTn();
void					initEnd();
void neighborhoodInit();
int						size() const { return points.size(); }
int						numLabels() const { return num_labels; }
int						dimensions() const { return attributes.size(); }
int						searchNN(const Point2d& seed,int k,std::vector<int>& result,std::vector<float>* result_d=0) const;
int						searchR(const Point2d& seed,float rad,int nn_max,std::vector<int>& result) const;
int						closest(int pid,float& d) const;							//Return point-id and distance to closest point to 'pid' in cloud
void					closestEdges(const Point2d& x, int pid,const Edge*& e1,float& d1,const Edge*& e2,float& d2) const;
float					interpolateDistMatrix(const Point2d& pix,float& certainty,float delta) const;
float					blendDistance(const Point2d& pix,const Triangle&) const;
int						hitTriangle(const Point2d& x) const;						//Return triangle (pid,e1,e2) that contains point x; false if no such triangle exists
bool					findTriangle(const Point2d& x,int& pid,const Edge*& e1,const Edge*& e2) const;
void					sortErrors();
void					computeFalseNegatives(int pid,bool norm=false);				//Compute false-negative error (relative_error[]) w.r.t. pid
void					computeFalseNegatives(const Grouping::PointGroup&,float range);
																					//Compute false-negative error (relative_error[]) w.r.t. entire given group
void					computeLabelMixing();										//Compute mixing of labels (in [0,1]) around each point
void					triangulate();												//Compute the exact Delaunay triangulation of the cloud
Grouping*				groupByLabel();												//Construct a grouping of this based on the (int) value of point-scalars
float					averageNeighborDist(int pid) const;							//Return average dist to geometric nbs of point 'pid'

std::vector<Point2d>	points;														//The cloud points
Point2d					min_p,max_p;												//Bounding box for points[]
std::vector<float>		class_data;												//Scalar data for points (can encode anything you want)
float					class_data_min,class_data_max;						//Range for class_data[]
ANNtree *kdt, *kdtn;														// KDT for points (2D/nD)
ANNpointArray			kdt_points;													//KDT for points (helper)
ANNpointArray kdtn_points;    // KDTn for original (nD) points (helper)
FullMatrix* distmatrix;   // Current Distance matrix
SparseMatrix*			edges;														//Edges of Delaunay triangulation of points: edges[i] contains all point-idxs that are connected to i via Delaunay edges
EdgeMatrix*				sorted_edges;												//Delaunay edges (as above), but sorted anticlockwise around each vertex. Useful for fast spatial point-between-edges search
SortedErrorMatrix*		sorted_errors;
std::vector<Triangle>	triangles;													//Delaunay triangulation of the point set
std::vector<TrisOfPoint> point2tris;												//Triangles sharing each point
std::vector<std::vector<float>*> attributes;										//n-dimensional attributes of points
std::vector<float>		attributes_min;
std::vector<float>		attributes_max;
// Complete 2D/nD neighborhood for each point, in ascending order of distance. Serves as a cache to speed-up calculations.
std::vector<ANNidxArray> neighborhood_matrix_2d;
std::vector<ANNidxArray> neighborhood_matrix_nd;

unsigned int*			buff_triangle_id;											//Per image-pixel, the triangle-id+1 (of the Delaunay triangle covering that pixel, if any), or 0 if no triangle there

float*					siteParam;													//Point parameterization (fboSize^2). siteParam(i,j) = point-id+1 if there's a point at (i,j), else 0
short*					siteFT;														//FT of points (fboSize^2)
float*					siteDT;														//DT of points (fboSize^2)
float					DT_max;														//Max value in siteDT[]
float					siteMax;
float					avgdist;													//Average inter-point distance in the cloud
int						fboSize;													//Size of various images used in here
int						num_labels;													//# different labels in the cloud

private:

	bool loadDistanceErrors(std::string, std::string);

};


//--------  Inlines  ----------------------------------------


inline int PointCloud::hitTriangle(const Point2d& p) const					//Return triangle-id that contains point x; returns -1 if no such triangle exists
{
	int tid = buff_triangle_id[int(p.x)+fboSize*int(p.y)];					//Index triangle-map to see what ID we have at that pixel
	if (!tid) return -1;													//Zero means no triangle
	return tid-1;															//Nonzero means triangle-id + 1
}
