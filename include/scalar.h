#ifndef SCALAR_H
#define SCALAR_H

#include <vector>
#include "pointcloud.h"
#include "scalarimage.h"
#include "view.h"
#include "gdrawingcloud.h"
#include "include/cpubundling.h"

// Base class for scalars

class Scalar : public View {

protected:
  std::vector<float> _values;
  Scalar(const PointCloud* pc, const std::string name) : View(pc, name) { _values.resize(pc->size()); }
  void normalize(float norm_range) {
    if (norm_range == 0.0f)
    {
      for (auto it = _values.begin(); it != _values.end(); ++it) {
        norm_range = std::max(norm_range, *it);
      }
    }
    if (norm_range < 1.0e-6) {
      norm_range = 1.0f;
    }
    for (auto it = _values.begin(); it != _values.end(); ++it) {
      *it /= norm_range;
    }
  }

public:
  const std::vector<float>& values() { return _values; }
  void updateImage() {
    ImageInterpolator::get(cloud).shepard((const float*)&_values[0], tex());
  }

  virtual void compute(float, int) = 0;

};

// Implementations

class AggregateError : public Scalar {

public:

  AggregateError(const PointCloud* pc) : Scalar(pc, "Aggregate error (all points)") {}

  void compute(float, int pid = -1);

};



class MissingNeighbors : public Scalar {

public:

  MissingNeighbors(const PointCloud* pc) : Scalar(pc, "Missing neighbors (selection)") {}

  void compute(float norm = 0.0, int pid = -1);

  void compute(float range, const Grouping::PointGroup& grp);

};



class LabelMixing : public Scalar {

public:

  LabelMixing(const PointCloud* pc) : Scalar(pc, "Label mixing") {}

  void compute(float norm = 0.0, int pid = -1);

};



class Neighborhood : public Scalar {

  int edg_k, edg_nd, edg_nearest_first, edg_excl;
	int tex_k, tex_nd, tex_nearest_first, tex_excl;
	void computeEdges();

public:
	int draw_edges;
	GraphDrawingCloud* bundling;   // Drawing of above
  Neighborhood(const PointCloud* pc);
  void compute(float norm = 0.0, int pid = -1);
  void options(GLUI*);

};


class DimRanking : public Scalar {

  int k, metric;

public:
  DimRanking(const PointCloud* pc);
  void compute(float norm = 0.0, int pid = -1);
  void compute(unsigned int k, int point_id) { this->k = k; compute(0.0f, point_id); }
  void options(GLUI*);

};

#endif
