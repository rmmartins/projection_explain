#pragma once


#include "include/field.h"
#include "include/Program.h"
#include "include/Helper.h"


class PointCloud;
struct Point2d;

// returns the full path to the file `fileName` in the same folder as the executable
inline std::string ResourcePath(std::string fileName) {
	return GetProcessPath() + "/../" + fileName;
}


class ImageInterpolator {

	static ImageInterpolator *instance;

	const PointCloud *cloud;
	tdogl::Program* gProgram [2];
	GLuint gVAO[2], gVBO[3], gTexture[3], gFBO;

	ImageInterpolator(const PointCloud *cloud);
	~ImageInterpolator();

public:
	static float rad_blur;
	static ImageInterpolator& get(const PointCloud *cloud) {
		if (instance == nullptr || instance->cloud != cloud) {
			delete instance;
			instance = new ImageInterpolator(cloud);
		}
		return *instance; 
	}
	void shepard(const float* point_data, const GLuint out_tex);

};






class ScalarImage
{
public:

			    ScalarImage(int width,int height);			//Ctor: init image
			   ~ScalarImage();								//Dtor: dealloc all owned by this
	void		interpolateDistMatrix(const PointCloud&,float);

	FIELD<float>* image;									//The raw scalar image
	FIELD<float>* certainty;								//Per-point certainty [0..1] for the above

	float		image_max;									//Max of the scalar data
};
