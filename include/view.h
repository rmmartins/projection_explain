#ifndef VIEW_H
#define VIEW_H

#include "glwrapper.h"
#include "pointcloud.h"
#include "scalarimage.h"

class View {

protected:
	const PointCloud* cloud;
	const int size;
	std::vector<float> _out_image;
	GLuint _tex;

	void updateTexture() {
		glBindTexture(GL_TEXTURE_2D, _tex);
  	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size, size, 0, GL_RGBA, GL_FLOAT, &_out_image[0]);
  	glBindTexture(GL_TEXTURE_2D, 0);
	}

public:
	const std::string name;

	View(const PointCloud* pc, const std::string name_) : cloud(pc), name(name_), size(pc->fboSize) {
		_out_image.resize(size*size*4);
		glGenTextures(1, &_tex);
		glBindTexture(GL_TEXTURE_2D, _tex);
  	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size, size, 0, GL_RGBA, GL_FLOAT, 0);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  	glBindTexture(GL_TEXTURE_2D, 0);
	}

	const GLuint tex() { return _tex; }

	virtual void updateImage() = 0;
	virtual void options(GLUI*) {}

};



class FalseNeighbors : public View {

	ScalarImage* _image;

public:
	FalseNeighbors(PointCloud* pc) : View(pc, "False neighbors (all points)") { _image = new ScalarImage(size, size); }
	~FalseNeighbors() { delete _image; }
	void updateImage() { return updateImage(0.0f, 10.0f); }
	void updateImage(float range, float distweight);

};



class DTView : public View {

public:
	DTView(PointCloud* pc) : View(pc, "Points' DT") {}
	void updateImage();

};

#endif
