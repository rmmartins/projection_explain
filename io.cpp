#include "include/glwrapper.h"

#include <math.h>
#include <algorithm>
#include <fstream>

#include "include/io.h"
#include "include/pointcloud.h"
#include "include/config.h"

#ifdef PLATFORM_WIN
#include <string>
#define isnan _isnan
#endif

#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>

using namespace std;
namespace fs = ::boost::filesystem;

std::vector<std::string> CMAPFILES;

std::vector<std::vector<float>> colormap;
int loaded_colormap = 0;
bool invert_colormap = false;

int checkLoadColormap(int cmap) {
  if (loaded_colormap != cmap) {
    ifstream cmap_file("./colormaps/" + CMAPFILES[cmap] + ".cmap");
    string line;
    bool is_first_line = true;
		char type;
		int i = 0;
    while (getline(cmap_file, line)) {
			// ignore comments in any line
      if (line[0] != '#') {
        if (is_first_line) {
					int num_colors;
					sscanf(line.c_str(), "%d%c", &num_colors, &type);
          colormap.resize(num_colors);
          is_first_line = false;
        }
				else {
					colormap[i].resize(3);
					if (type == 'f') {
						sscanf(line.c_str(), "%f %f %f\n", &colormap[i][0], &colormap[i][1], &colormap[i][2]);
					}
					else {
						int color[3];
						sscanf(line.c_str(), "%d %d %d\n", &color[0], &color[1], &color[2]);
						for (int j = 0; j < 3; ++j)
							colormap[i][j] = (float)color[j] / 255.0f;
					}
					++i;
				}
      }
    }
    loaded_colormap = cmap;
  }
  return loaded_colormap;
}

namespace projwiz {

	void float2rgb(float value,float& R,float& G,float& B,bool color_or_grayscale)
	{
		if (isnan(value))
		{
			R = G = B = 1;
			printf("The color of a NaN value was requested. This should not happen.\n");
		} else
			if (!invert_colormap)
				value = 1.0f - value;
			if (!color_or_grayscale)
			{
				R = G = B = value;
			}
			else
			{
				const float dx=0.8f;
				switch (loaded_colormap)
				{
					case CMAP_RAINBOW:
						value = (6-2*dx)*value+dx;
						R = max(0.0f,(3-(float)fabs(value-4)-(float)fabs(value-5))/2);
						G = max(0.0f,(4-(float)fabs(value-2)-(float)fabs(value-4))/2);
						B = max(0.0f,(3-(float)fabs(value-1)-(float)fabs(value-2))/2);
						break;
					default:
						//checkLoadColormap(current_cmap);
						float index = value * ((float)colormap.size() - 1);
						unsigned int index_i = (unsigned int)(index);
						float delta = index - ((float)index_i);
						R = (float)colormap[index_i][0];
						G = (float)colormap[index_i][1];
						B = (float)colormap[index_i][2];
						if (index_i < (colormap.size() - 1))
						{
							R += (((float)(colormap[index_i + 1][0] - colormap[index_i][0])) * delta);
							G += (((float)(colormap[index_i + 1][1] - colormap[index_i][1])) * delta);
							B += (((float)(colormap[index_i + 1][2] - colormap[index_i][2])) * delta);
						}
						//R /= 255.0f;
						//G /= 255.0f;
						//B /= 255.0f;
						break;
				}
			}
	}

}

void glutDrawString(const char* s)
{
  while (*s) glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10,*s++);
}


void glVertex2f(const Point2d& p)
{
  glVertex2f(p.x,p.y);
}

void drawSplat(const Point2d& p,float rad)		//Draw a splat (current texture) on a radius of 'rad' pixels centered at 'c'
{
  glTexCoord2f(0,0);
  glVertex2f(p.x-rad,p.y-rad);
  glTexCoord2f(1,0);
  glVertex2f(p.x+rad,p.y-rad);
  glTexCoord2f(1,1);
  glVertex2f(p.x+rad,p.y+rad);
  glTexCoord2f(0,1);
  glVertex2f(p.x-rad,p.y+rad);
}

void setTexture (GLuint tex_name, int tex_interp) {
  glActiveTexture(GL_TEXTURE0);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, tex_name);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (tex_interp)?GL_LINEAR:GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (tex_interp)?GL_LINEAR:GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
}

void drawCircle(const Point2d& c,float rad)
{
  const int N = 30;

  glBegin(GL_LINE_LOOP);
  for(int i=0;i<N;++i)
  {
    float alpha = 2.0f*(float)M_PI*float(i)/(float)N;
    float x = rad*cos(alpha);
    float y = rad*sin(alpha);
    glVertex2f(c.x+x,c.y+y);
  }
  glEnd();

}

// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
void get_all(const fs::path& root, const string& ext, vector<string>& ret)
{
  if (!fs::exists(root)) return;

  if (fs::is_directory(root))
  {
    fs::recursive_directory_iterator it(root);
    fs::recursive_directory_iterator endit;
    while(it != endit)
    {
      if (fs::is_regular_file(*it) && it->path().extension() == ext)
      {
        ret.push_back(it->path().filename().string());
      }
      ++it;
    }
  }
}

std::vector<std::string> getColormaps() {
  if (CMAPFILES.empty()) {
    // Load colormap names
    cout << "INIT: Loading colormaps... ";
    vector<string> ret;
    get_all("./colormaps", ".cmap", ret);
    CMAPFILES.push_back("Rainbow"); // This one is not on a file, it's a function
	for (auto i = ret.begin(); i != ret.end(); ++i) {
      CMAPFILES.push_back(i->substr(0, i->length() - 5));
    }
    cout << "ok." << endl;
  }
  return CMAPFILES;
}
