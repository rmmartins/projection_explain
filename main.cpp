#include "include/config.h"
#include "include/skelft.h"
#include "include/vis.h"
#include "include/myrandom.h"
#include "include/pointcloud.h"
#include "include/sparsematrix.h"
#include "include/fullmatrix.h"
#include "include/grouping.h"
#include "include/io.h"

#include <math.h>
#include <iostream>
#include <string>
#include <time.h>

#ifdef PLATFORM_OSX
#include <cutil_inline.h>    // includes cuda.h and cuda_runtime_api.h
#include <cutil_gl_inline.h>
#else
#include <cuda.h>
#include <cuda_runtime_api.h>
#endif

#include <cudaGL.h>

using namespace std;



//-------------------------------------------------------------------------------------------------------

/*
void initPointCloud(PointCloud*,int,int);
void testPointCloud(PointCloud*,int);
*/

//-------------------------------------------------------------------------------------------------------


RadialGrouping*     visual_clustering = 0;
Grouping*			labelg = 0;




int main(int argc,char **argv)
{
  int   NP		= 10;											//#particles to use (default)
  int	  fboSize   = 512;	
  bool  load_nd   = false;
  char *pointfile, *projname;
  bool error = false;

  if (argc < 2) {
    cout << "ERROR: specify at least the points file and projection name (with -f)." << endl;
    error = true;
  } else {
    for (int ar=1;ar<argc;++ar)
    {
      string opt = argv[ar];
      if (opt=="-n")
      {
        ++ar;
        NP = atoi(argv[ar]);
      }
      else if (opt=="-f")
      {
        ++ar;
        pointfile = argv[ar];
        if (ar+1<argc && argv[ar+1][0]!='-')
        {
          ++ar;
          projname = argv[ar];
        }		
      }
      else if (opt=="-i")
      {
        ++ar;
        fboSize = atoi(argv[ar]);
      }
      else if (opt=="-d")
      {
        load_nd = true;
      }
      else {
        cout << "ERROR: Wrong argument/option -- \"" << opt << "\"." << endl;
        error = true;
        break;
      }
    }
  }

  if (error) {
    cout << "OPTIONS:" << endl;
    cout << "  -f file proj           -- file and projection name (\"lamp\", \"lsp\", etc.)" << endl;
    cout << "  -i N                   -- screen of NxN pixels (default: 512)" << endl;
    cout << "  -d                     -- load .nd file (default: false)" << endl;
    cout << "  -n N                   -- total of N points on the projection" << endl;
    cout << "EXAMPLE:" << endl;
    cout << "  projwiz -f segm lamp   -- reads files \"segm.lamp.2d\" and \"segm.lamp.err\"" << endl;
    return(-1);
  }

#ifdef PLATFORM_OSX	
  cudaGLSetGLDevice(0);												//Let CUDA communicate with OpenGL
#endif
	
    skelft2DInitialization(fboSize);									//Initialize CUDA DT/FT API

	PointCloud*		cloud = new PointCloud(fboSize);					//2. Point cloud

	if (pointfile)														//Read data from file:
	{
	   bool ok = cloud->loadPex(pointfile,projname,load_nd);
	   if (!ok) 
	   {
		  cout<<"Cannot read given data, aborting"<<endl;
		  return 1;	
	   }
	} 
	else																//Generate synthetic data:
	{
		/*
	   if (NP>0)
		  initPointCloud(cloud,fboSize,NP);								//Initialize with random point distribution
	   else		
	      testPointCloud(cloud,fboSize);								//Create simple point-cloud with 3 points (for testing)
	  */
	}

	cloud->initEnd();													//Finalize point cloud creation, once all points are added		
	RadialGrouping* rg = new RadialGrouping(cloud);						//Remove trivial exact-overlaps of points (since they create stupid visualization problems)
	RadialGrouping* crs = rg->coarsen(0);								//From now on, use only the cleaned-up points	
	PointCloud* clean_cloud = crs->cloud;								//

	clean_cloud->avgdist = point_influence_radius;	

	RadialGrouping* vg = new RadialGrouping(clean_cloud);				//Make an engine to coarsen the cloud; We'll use it further for simplified visualizations.
	//StronglyConnectedGrouping* vg = new StronglyConnectedGrouping(clean_cloud);
	//vg->build();
	visual_clustering = vg;

	labelg = clean_cloud->groupByLabel();	
				
    Display* dpy = new Display(fboSize,clean_cloud,argc,argv);			//Initialize visualization engine																		
		dpy->selected_point_id = selected_point_id;

    glutMainLoop(); 	

  skelft2DDeinitialization(); 
  delete vg;
  delete labelg;
//  delete rg;
  delete dpy;
  delete cloud;
  return 0;
}


/*
void testPointCloud(PointCloud* cloud,int size)
{
	const float t = 0.05;

	cloud->points.resize(3);
	cloud->class_data.resize(3);
	cloud->class_data_min = 1.0e+8;
	cloud->class_data_max = -1.0e+8;
	cloud->distmatrix = new PointCloud::DistMatrix(3);
		
	float wX = size, wY = size;
	
	cloud->points[0] = Point2d(t*wX,t*wY); 
	cloud->class_data[0] = 0;
	cloud->points[1] = Point2d((1-t)*wX,t*wY); 
	cloud->class_data[1] = 0;
	cloud->points[2] = Point2d((1-t)*wX,(1-t)*wY); 
	cloud->class_data[2] = 0;

	(*cloud->distmatrix)(0,1) = (*cloud->distmatrix)(1,0) = 1;
	(*cloud->distmatrix)(0,2) = (*cloud->distmatrix)(2,0) = 0.5;
	(*cloud->distmatrix)(1,2) = (*cloud->distmatrix)(2,1) = 0;
	

	for(int i=0;i<3;++i)
	{
			const Point2d& np = cloud->points[i]; 
			const float& val = cloud->class_data[i];
			if (cloud->class_data_min>val) cloud->class_data_min = val;
			if (cloud->class_data_max<val) cloud->class_data_max = val;
			cloud->min_p.x = std::min(cloud->min_p.x,np.x);
			cloud->min_p.y = std::min(cloud->min_p.y,np.y);
			cloud->max_p.x = std::max(cloud->max_p.x,np.x);
			cloud->max_p.y = std::max(cloud->max_p.y,np.y);		
	}
}	

void initPointCloud(PointCloud* cloud,int size,int NP)					//Some test-initialization of a point cloud
{
	const int   NNBS = 1;												//Number of neighborhoods/clusters to make
	const float NMAX = ceil(float(NP)/NNBS);							//Max # points in a 'cluster'
	const float t = 0.05;

	randinit(clock());													//Initialize random generator to hopefully something random itself..	
	
	cloud->points.resize(NP);
	cloud->class_data.resize(NP);
	cloud->class_data_min = 1.0e+8;
	cloud->class_data_max = -1.0e+8;
	cloud->distmatrix = new PointCloud::DistMatrix(NP);
	
	
	float wX = size, wY = size;
	float dX = t*wX, dY = t*wY;
	wX -= 2*dX; wY -= 2*dY;
	
	float diag  = sqrt(wX*wX+wY*wY);
	float R_max = sqrt(wX*wY/M_PI);

	bool ready = false;	
	int  ngen  = 0;
	for(int i=0;!ready && i<NP;++i)
	{
		int ii = dX + myrandom()*wX;										//Center of current neighborhood
		int jj = dY + myrandom()*wY;
		
		int NN = NMAX*(0.5+0.5*myrandom());									//How many points to add to current neighborhood
		if (NN==0) NN=1;													//We want at least one point
		
		float R_nb = R_max*(0.3 + myrandom()*0.7);							//Radius of current neighborhood: around R_max
		for(int i=0;!ready && i<NN;++i)										//Generate current neighborhood:
		{
			float alpha  = myrandom()*2*M_PI;								//Random point in current neighborhood (random angle [0,2*M_PI], random radius [0,R_max])
			float radius = myrandom()*R_nb;
		
			int II = ii + radius*sin(alpha);
			int JJ = jj + radius*cos(alpha);			
			if (II<1 || JJ<1 || II>wX-2 || JJ>wY-2) continue;				//Be sure not to generate points on image border (simplifies many calculations later)
		
			Point2d& np = cloud->points[ngen]; 
			np = Point2d(II,JJ);
			float val = myrandom();
			cloud->class_data[ngen] = val;
			if (cloud->class_data_min>val) cloud->class_data_min = val;
			if (cloud->class_data_max<val) cloud->class_data_max = val;

			cloud->min_p.x = std::min(cloud->min_p.x,np.x);
			cloud->min_p.y = std::min(cloud->min_p.y,np.y);
			cloud->max_p.x = std::max(cloud->max_p.x,np.x);
			cloud->max_p.y = std::max(cloud->max_p.y,np.y);		
			
			++ngen;
			ready = ngen == NP;
		}	
	}

	for(int i=0;i<NP;++i)
	  for(int j=i;j<NP;++j)
	  {
		float val = myrandom();
		(*cloud->distmatrix)(i,j) = val;
		(*cloud->distmatrix)(j,i) = val;
	  }
}	
*/
