#ifdef PLATFORM_LINUX
#include <stdio.h>
#include <string.h>
#endif

#ifdef PLATFORM_WIN
#define isnan _isnan
#endif

#include "include/pointcloud.h"
#include "include/skelft.h"
#include "include/sparsematrix.h"
#include "include/fullmatrix.h"
#include "include/sortederrmatrix.h"
#include "include/grouping.h"

#include <cuda_runtime_api.h>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <deque>
#include <cmath>

#define REAL double									//Needed for Triangle; note also that Triangle is a C API

#ifndef TRILIBRARY
#define TRILIBRARY
#endif
#ifndef NO_TIMER
#define NO_TIMER
#endif
#define ANSI_DECLARATORS
#define VOID void

extern "C" {
#include "triangle.h"
}


using namespace std;



float Point2d::edgeAngle(const Point2d& p, const Point2d& q)
{
  const Point2d& r  = q-p;
  float n           = r.norm();
  if (n<1.0e-4) return -1;							//Cannot determine angle, edge is of zero length

  float cos_r       = r.x/n;
  float sin_r       = r.y/n;
  float alpha       = acos(cos_r);
  if (sin_r<0)  alpha = 2*(float)M_PI - alpha;
  return alpha;
}

float Point2d::angle(const Point2d& r)
{
  float n           = r.norm();
  if (n<1.0e-4) return -1;							//Cannot determine angle, edge is of zero length

  float cos_r       = r.x/n;
  float sin_r       = r.y/n;
  float alpha       = acos(cos_r);
  if (sin_r<0)  alpha = 2*(float)M_PI - alpha;
  return alpha;
}

Point2d Point2d::center(const Point2d& a,const Point2d& b,const Point2d& c)
{
  Point2d ab = b-a; ab.normalize();
  Point2d ac = c-a; ac.normalize();
  Point2d cb = b-c; cb.normalize();
  Point2d ca = a-c; ca.normalize();

  Point2d  A = (ab+ac); A.normalize();
  Point2d  C = (ca+cb); C.normalize();

  float t2 = (A.x*(c.y-a.y)-A.y*(c.x-a.x))/(A.y*C.x-A.x*C.y);

  Point2d r;
  r.x = C.x*t2 + c.x;
  r.y = C.y*t2 + c.y;

  return r;
}

//-------------------------------------------------------------------------------------------------------------------------


PointCloud::PointCloud(int size): kdt(0),kdtn(0),kdt_points(0),kdtn_points(0),siteMax(0),fboSize(size),DT_max(0),
  class_data_min(0),class_data_max(0),
  avgdist(0),num_labels(0)
{
  cudaMallocHost((void**)&siteFT,size*size*2*sizeof(short));
  cudaMallocHost((void**)&siteParam,size*size*sizeof(float));
  cudaMallocHost((void**)&siteDT,size*size*sizeof(float));

  buff_triangle_id = new unsigned int[fboSize*fboSize];
}

PointCloud::~PointCloud()
{
  if (kdt) { delete kdt; kdt=0; }
  if (kdtn) { delete kdtn; kdtn=0; }
  if (kdt_points) annDeallocPts(kdt_points);
  if (kdtn_points) annDeallocPts(kdtn_points);
  delete edges;
  delete sorted_edges;
  delete distmatrix;
  delete sorted_errors;
  while (!attributes.empty()) delete attributes.back(), attributes.pop_back();

  delete[] buff_triangle_id;

  cudaFreeHost(siteFT);
  cudaFreeHost(siteParam);
  cudaFreeHost(siteDT);
}

void PointCloud::makeKDT()
{
  delete kdt;														//Deallocate whatever we hold currently
  if (kdt_points) annDeallocPts(kdt_points);

  kdt_points = annAllocPts(points.size(), 2);						//Put all vertices in the ANN search structure

  for(unsigned int i=0;i<points.size();i++)								//Copy surf vertices in an ANN-compatible structure
  {
    kdt_points[i][0]=points[i].x;
    kdt_points[i][1]=points[i].y;
  }

  kdt = new ANNtree(kdt_points, points.size(), 2, 5);
}

void PointCloud::makeKDTn()
{
  // Deallocate whatever we hold currently
  delete kdtn;
  if (kdtn_points) annDeallocPts(kdtn_points);

  // The cloud may have been "coarsed", so we count the original number of points
  int NP = (*attributes[0]).size();
  int ND = attributes.size();

  // Allocate and put original (nD) data in an ANN-compatible structure
  kdtn_points = annAllocPts(NP, ND);
  for (int i = 0; i < NP; i++)
    for (int j = 0; j < ND; j++)
      kdtn_points[i][j] = (*attributes[j])[i];    // original data is stored "attribute-first"

  kdtn = new ANNtree(kdtn_points, NP, ND);
}

int PointCloud::searchR(const Point2d& seed,float rad,int nn_max,vector<int>& result) const
{
  ANNcoord query_pt[2];
  static ANNidx   nn_idx[10000];

  query_pt[0] = seed.x;
  query_pt[1] = seed.y;

  int nn = kdt->annkFRSearch(query_pt,rad*rad,nn_max,nn_idx);				//Search up to nn_max points within rad from seed.

  if (nn>nn_max) nn=nn_max;												//If there are more than nn_max points in the ball, beware that
  //we only returned nn_max of them.
  result.resize(nn);
  for(int i=0;i<nn;++i)
    result[i] = nn_idx[i];

  return nn;
}


int PointCloud::closest(int pid,float& d) const
{
  ANNcoord query_pt[2];
  ANNidx   nn_idx[2];
  ANNdist  nn_dist[2];

  query_pt[0] = points[pid].x;
  query_pt[1] = points[pid].y;

  kdt->annkSearch(query_pt,2,nn_idx,nn_dist);

  d = (float)sqrt(nn_dist[1]);

  return (nn_idx[1]!=pid)? nn_idx[1]:nn_idx[0];
}

int PointCloud::searchNN(const Point2d& seed,int k,vector<int>& result,vector<float>* result_dist) const
{
  ANNcoord query_pt[2];
  static ANNidx   nn_idx[10000];
  static ANNdist  nn_dist[10000];

  query_pt[0] = seed.x;
  query_pt[1] = seed.y;

  kdt->annkSearch(query_pt,k,nn_idx,nn_dist);

  result.resize(k);
  if (result_dist) result_dist->resize(k);
  for(int i=0;i<k;++i)
  {
    result[i] = nn_idx[i];
    if (result_dist) (*result_dist)[i] = (float)nn_dist[i];
  }

  return k;
}

void PointCloud::triangulate()
{
  triangulateio ti,to;
  double pts[50000];													//In: 2D points to be triangulated
  int tris[50000];														//Out: triangles created by the 2D triangulation
  int tedges[500000];													//Out: edges of triangulation

  ti.pointlist				   = pts;									//Set up triangulation of projected points
  ti.pointmarkerlist          = 0;										//
  ti.numberofpointattributes  = 0;										//
  ti.numberofpoints		   = points.size();

  to.trianglelist             = tris;									//All we want are the triangles..
  to.edgelist				   = tedges;								//..and edges
  to.pointmarkerlist          = 0;										//

  for(int i=0;i<ti.numberofpoints;++i)
  {
    pts[2*i]   = points[i].x;
    pts[2*i+1] = points[i].y;
  }

  ::triangulate((char*)"zePBNQYY",&ti,&to,0);									//Call Triangle-lib to do the triangulation of the projected skel-points

  triangles.resize(to.numberoftriangles);								//Get triangles:
  point2tris.resize(points.size());									//In the same time, construct point2tris[]
  for(int i=0;i<to.numberoftriangles;++i)
  {
    int t = 3*i;
    int a = to.trianglelist[t];
    int b = to.trianglelist[t+1];
    int c = to.trianglelist[t+2];
    triangles[i] = Triangle(a,b,c);

    point2tris[a].insert(i);
    point2tris[b].insert(i);
    point2tris[c].insert(i);
  }

  for(int i=0;i<to.numberofedges;++i)									//Get edges:
  {
    int i1 = tedges[2*i], i2 = tedges[2*i+1];
    (*edges)(i1,i2) = 1;
    (*edges)(i2,i1) = 1;
  }
}




struct EdgeCompare													//Compares two edges vs their angles [0..M_PI] with the +x axis
{																	//Used to sort edges anticlockwise
  EdgeCompare() {}
  bool operator() (const PointCloud::Edge& ei,const PointCloud::Edge& ej)
  { return ei.angle < ej.angle; }
};


void PointCloud::sortErrors()										//For each matrix row (point in cloud), sort dist-errors to all other points.
{																	//Like this, it's next easy to find out, for any point, which are its most
  int NP = points.size();											//evident false positives (large negative dist-errors) and false negatives
  sorted_errors = new SortedErrorMatrix(NP);						//(large positive dist-errors)

  for(int i=0;i<NP;++i)
  {
    const DistMatrix::Row& row  = (*distmatrix)(i);
    SortedErrorMatrix::Row&  srow = (*sorted_errors)(i);

    int idx = 0;
    for(DistMatrix::Row::const_iterator it=row.begin();it!=row.end();++it,++idx)
    {
      float val = *it;
      srow.insert(make_pair(val,idx));
    }
  }
}


void PointCloud::initEnd()
{
  int NP = points.size();

  memset(siteParam,0,fboSize*fboSize*sizeof(float));				//Site parameterization:
  //0: no site at current pixel
  for(int i=0;i<NP;++i)											//i: site i-1 at current pixel
  {
    const Point2d& p = points[i];
    if (isnan(p.x) || isnan(p.y)) continue;
    siteParam[int(p.y)*fboSize+int(p.x)] = (float)i+1;
  }

  siteMax = (float)NP+1;

  skelft2DFT(siteFT,siteParam,0,0,fboSize,fboSize,fboSize);		//Compute FT of the sites
  skelft2DDT(siteDT,0,0,fboSize,fboSize);							//Compute DT of the sites (from the resident FT)


  edges = new SparseMatrix(NP);									//Allocate Delaunay adjacency matrix
  sorted_edges = new EdgeMatrix(NP);								//Allocate ordered Delaunay adj. matrix (edges ordered by angle anticlockwise)

  DT_max = 0;														//Compute max of DT
  for(int i=0;i<fboSize*fboSize;++i)
  {
    float dt = siteDT[i];
    if (dt>DT_max) DT_max = dt;
  }

  triangulate();													//Compute Delaunay triangulation of this

  EdgeCompare edge_comp;											//Used to compare edges vs their angles
  for(int i=0;i<NP;++i)											//Construct sorted_edges[]: Delaunay edges[], but sorted anticlockwise around each point
  {
    const SparseMatrix& ed       = *edges;
    const SparseMatrix::Row& row = ed(i);						//This is what we want to sort
    EdgeMatrix::Row&		orow = (*sorted_edges)(i);			//This is where the sorted output goes
    const Point2d&             p = points[i];					//This is the current vertex, for which we'll sort edges

    orow.resize(row.size());									//Make room for the sorted output
    int j=0;
    for(SparseMatrix::Row::const_iterator it=row.begin();it!=row.end();++it)
    {
      const int	  pid = it->first;							//Find the angle [0..M_PI] of edge (p,points[it->first]) with the +x axis
      float alpha = Point2d::edgeAngle(p,points[pid]);
      orow[j++] = Edge(pid,alpha);							//Copy the unsorted data to the sorted output, prior to sort
    }

    std::sort(orow.begin(),orow.end(),edge_comp);				//Sort orow[] using the anticlockwise edge order
  }

  makeKDT();
  if (dimensions() > 0)
    makeKDTn();
  else {
    cout << "WARNING: The original coordinates (nD) weren't loaded; ";
    cout << "most neighborhood views will not work." << endl;
  }

  hash_set<float> lbls;
  avgdist = 0;													//Compute average distance points in the cloud
  for(int i=0;i<NP;++i)											//For this, we estimate the avg radius of the NSZ-nb, with a small NSZ
  {																//(for some reason, NSZ=approx 5 gives good results)
    const int NSZ = 5;
    vector<int> nn; vector<float> nnd;
    searchNN(points[i],NSZ,nn,&nnd);
    float dist = sqrt(nnd[NSZ-1]);
    avgdist += dist;
    lbls.insert(class_data[i]);
  }
  avgdist /= NP;
  num_labels = lbls.size();

  sortErrors();

  // Initialize neighborhood cache
  neighborhood_matrix_2d.resize(NP);
  ANNdistArray dists = new ANNdist[NP]; // ignored, but needed anyway
  ANNpoint query_pt_2d = annAllocPt(2);
  // For each point i...
  for (int i = 0; i < NP; ++i) {
    // ...cache its entire 2D neighborhood.
    query_pt_2d[0] = points[i].x;
    query_pt_2d[1] = points[i].y;
    ANNidxArray nn_idx_2d = new ANNidx[NP];
    kdt->annkSearch(query_pt_2d, NP, nn_idx_2d, dists);
    neighborhood_matrix_2d[i] = nn_idx_2d;
  }
  annDeallocPt(query_pt_2d);
  // The nD neighborhood can only be calculated if nD file was loaded.
  if (dimensions() > 0) {
    unsigned int ND = attributes.size();
    neighborhood_matrix_nd.resize(NP);
    ANNpoint query_pt_nd = annAllocPt(ND);
    // For each point i...
    for (int i = 0; i < NP; ++i) {
      // ...cache its entire nD neighborhood.
      for (unsigned int j = 0; j < ND; j++) {
        query_pt_nd[j] = (*attributes[j])[i];
      }
      ANNidxArray nn_idx_nd = new ANNidx[NP];
      kdtn->annkSearch(query_pt_nd, NP, nn_idx_nd, dists);
      neighborhood_matrix_nd[i] = nn_idx_nd;
    }
    annDeallocPt(query_pt_nd);
  }
  delete[] dists;
}


Grouping* PointCloud::groupByLabel()								//Construct grouping of points in this based on equal-label
{
  SimpleGrouping* grp = new SimpleGrouping(this);					//Make new grouping based on point-labeling of this
  map<int,int> gkey2gidx;											//Maps the group-ids to 0-based ids

  int NG=0,NP=size();
  for(int i=0;i<NP;++i)											//Read all points:
  {
    int gkey = int(class_data[i]);							//See if we have a new group-id
    if (gkey2gidx.find(gkey)==gkey2gidx.end())					//If so, map it to a 0-based increasing integer
    {
      gkey2gidx.insert(make_pair(gkey,NG)); ++NG;
    }
  }

  grp->resize(NG);												//We now know how many groups we have to make
  for(int i=0;i<NP;++i)											//Scan the points and add them to their right groups
  {
    int gkey = int(class_data[i]);
    int gid  = gkey2gidx.find(gkey)->second;
    grp->group(gid).insert(i);
  }

  return grp;
}



bool PointCloud::loadPex(const char* file,const char* proj,bool load_nd)	//Load triplet of Pex files: nD, 2D, error
{
  const float t = 0.04f;											//Border between the points and image-border (needed for safe DT computations)

  string fnd = file; fnd += ".nd";								//n-dimensional points file
  string f2d = file;												//2-dimensional projection file
  if (proj) { f2d += "."; f2d += proj; }
  f2d += ".2d";

  char line[1024];

  FILE* fp = fopen(f2d.c_str(),"r");								//1. Read 2D projections:
  if (!fp)
  {
    cout<<"Error: Cannot open "<<f2d<<endl;
    return false;
  }

  fgets(line,1024,fp);											//Skip first line 'DY'
  int NP;
  fscanf(fp,"%d",&NP);											//Get #points in file
  int dim;
  fscanf(fp,"%d",&dim);											//Get point dimensions (should be 2)
  if (dim!=2)
  {
    cout<<"Warning: 2D projection dimension="<<dim<<", expected 2"<<endl;
  }

  points.resize(NP);
  class_data.resize(NP);
  class_data_min = min_p.x = min_p.y = 1.0e+8;
  class_data_max = max_p.x = max_p.y = -1.0e+8;

  for(int i=0;i<NP;++i)											//Read all 2D point projections:
  {
    Point2d& p = points[i];
    fscanf(fp,"%*[^;];%f;%f;%f",&p.x,&p.y,&class_data[i]);	//REMARK: apparently, first item (point-ID) can be a string..
    class_data_min = std::min(class_data_min,class_data[i]);
    class_data_max = std::max(class_data_max,class_data[i]);
    min_p.x = std::min(min_p.x,p.x);
    min_p.y = std::min(min_p.y,p.y);
    max_p.x = std::max(max_p.x,p.x);
    max_p.y = std::max(max_p.y,p.y);
  }
  fclose(fp);

  Point2d range(max_p-min_p);

  if (load_nd)													//4. Read the nD data values:
  {
    fp = fopen(fnd.c_str(),"r");
    fgets(line,1024,fp);											//Skip first line 'DY'
    int NP_n;
    fscanf(fp,"%d",&NP_n);											//Get #points in file
    if (NP_n!=NP)
    {
      cout<<"Error: "<<NP_n<<" nD points, "<<NP<<" 2D points"<<endl;
      return false;
    }

    int ND;
    fscanf(fp,"%d",&ND);											//Get nD point dimensions
    attributes.resize(ND);											//Allocate space for attributes
    attributes_min.resize(ND);
    attributes_max.resize(ND);
    for(int i=0;i<ND;++i)
    {
      attributes[i] = new vector<float>(NP);
      attributes_min[i] = 1.0e+6;
      attributes_max[i] = -1.0e+6;
    }

		fscanf(fp,"\n");

    char pid_nd[128];
    for(int i=0;i<NP;++i)											//Read all n-D points:
    {
      fscanf(fp,"%[^;];",pid_nd);									//REMARK: Apparently, point-ID can be a string..
      for(int j=0;j<ND;++j)										//Read all n-D dimensions for point 'i'
      {
        float dim_j;
        fscanf(fp,"%f;",&dim_j);
        (*attributes[j])[i] = dim_j;
        attributes_min[j] = std::min(attributes_min[j],dim_j);
        attributes_max[j] = std::max(attributes_max[j],dim_j);
      }

      float label;												//Sanity check: label must be the same in 2D and nD
      fscanf(fp,"%f",&label);
      if (label!=class_data[i])
      {
        cout<<"Error: point "<<i<<"("<<pid_nd<<") has label "<<label<<" in nD and label "<<class_data[i]<<" in 2D"<<endl;
        return false;
      }
    }
    fclose(fp);

		// Always load nD file first, then the errors
		loadDistanceErrors(file, proj);

		// Normalize read points within available image space (fboSize).
		// It's important to do this only AFTER calculating the distance errors, or
		// else the errors will be wrong.
		for(int i=0;i<NP;++i) {
			Point2d& p = points[i];
			p.x = (p.x-min_p.x)/range.x*(1-2*t)*fboSize + t*fboSize;
			p.y = (p.y-min_p.y)/range.y*(1-2*t)*fboSize + t*fboSize;
		}

		for(int i=0;i<NP;++i) {
			Point2d& p = points[i];
			min_p.x = std::min(min_p.x,p.x);
			min_p.y = std::min(min_p.y,p.y);
			max_p.x = std::max(max_p.x,p.x);
			max_p.y = std::max(max_p.y,p.y);
		}
	}

	return true;
}



void PointCloud::closestEdges(const Point2d& x,int pid,const Edge*& e1,float& d1,const Edge*& e2,float& d2) const
{
  const Point2d& p = points[pid];							//'center' of the edge-fan we search into
  float      alpha = Point2d::edgeAngle(p,x);				//angle of line from p to current x

  const EdgeMatrix::Row& row = (*sorted_edges)(pid);		//all edges in edge-fan

  if (row.size()<2)
  {
    cout<<"ERROR: point "<<pid<<" has only "<<row.size()<<" edges"<<endl;
    e1 = e2 = 0;
    return;
  }

  unsigned int i=0;
  for(;i<row.size();++i)									//search where the current angle falls (note that row is sorted on angles..)
  {
    if (alpha <= row[i].angle) break;
  }

  i = i % row.size();
  int j = i-1;
  if (j<0) j += row.size();								//next edge after edge 'i'

  e2 = &row[i];
  e1 = &row[j];
  d2 = x.distance2line(p,points[e2->pid]);
  d1 = x.distance2line(p,points[e1->pid]);
}



float PointCloud::blendDistance(const Point2d& pix,const Triangle& tr) const
{
  int i0 = tr(0);
  int i1 = tr(1);
  int i2 = tr(2);

  const Point2d& p0 = points[i0];						//closest cloud-point to 'pix'
  const Point2d& p1 = points[i1];
  const Point2d& p2 = points[i2];

  float d1 = pix.distance2line(p0,p1);
  float d2 = pix.distance2line(p0,p2);
  float d3 = pix.distance2line(p1,p2);
  float tmin = min(d1,min(d2,d3));					//DT of triangle

  Point2d c = Point2d::center(p0,p1,p2);
  float dmin = c.dist(pix);							//DT of center
  float B = 0.5f*(((dmin)? min(tmin/dmin,1.0f):1.0f) + ((tmin)? max(1-dmin/tmin,0.0f):0.0f));
  return pow(B,0.5f);
}


/*
   bool PointCloud::findTriangle(const Point2d& x,int& pid,const Edge*& e1,const Edge*& e2) const
   {
   vector<int> nbs;
   searchNN(x,1,nbs);
   pid = nbs[0];															//Closest point to 'x'

   std::deque<int> q;
   std::vector<bool> visited(triangles.size());

   const TrisOfPoint& fan = point2tris[pid];
   for(TrisOfPoint::const_iterator it=fan.begin();it!=fan.end();++it)
   {   }

   while(q.size())
   {
   int t  = *(q.begin()); q.pop_front();
   const Triangle& tr = triangles[t];
   int p0 = tr.idx[0];
   int p1 = tr.idx[1];
   int p2 = tr.idx[2];
   if (x.inTriangle(points[p0],points[p1],points[p2]))
   {
   return hitTriangle(x,pid,e1,e2);
   }

   const TrisOfPoint& f0 = point2tris[p0];
   for(TrisOfPoint::const_iterator it=f0.begin();it!=f0.end();++it)
   if (!visited[*it]) { q.push_back(*it); visited[*it]=true; }

   const TrisOfPoint& f1 = point2tris[p1];
   for(TrisOfPoint::const_iterator it=f1.begin();it!=f1.end();++it)
   if (!visited[*it]) { q.push_back(*it); visited[*it]=true; }

   const TrisOfPoint& f2 = point2tris[p2];
   for(TrisOfPoint::const_iterator it=f2.begin();it!=f2.end();++it)
   if (!visited[*it]) { q.push_back(*it); visited[*it]=true; }

   }

   return false;
   }
   */




inline float errorDistWeighting(float d,float delta)
{
  return (d<delta)? 1 : exp(-0.3f*(d-delta)*(d-delta));
}


float PointCloud::interpolateDistMatrix(const Point2d& pix,float& certainty,float delta) const
{
  int tid = hitTriangle(pix);
  if (tid<0)										//pix outside the point cloud triangulation: nothing to show there, really
  {
    certainty = 0;
    return 0;
  }

  const Point2d* p[3]; float d[3]; int i[3]; float w[3];
  const DistMatrix& dm = *distmatrix;
  const Triangle& tr = triangles[tid];

  i[0] = tr(0);
  i[1] = tr(1);
  i[2] = tr(2);

  p[0] = &points[i[0]];							//p1 = other end of edge (p,p1)
  p[1] = &points[i[1]];							//p2 = other end of edge (p,p2)
  p[2] = &points[i[2]];
  //Get edge weights:
  w[0] = -std::min(dm(i[2],i[0]),0.0f);			//As we ONLY want to emphasize false-positives here,
  w[1] = -std::min(dm(i[2],i[1]),0.0f);			//so we simply eliminate false-negatives from this data.
  w[2] = -std::min(dm(i[0],i[1]),0.0f);

  d[0] = pix.distance2line(*p[2],*p[0]);
  d[1] = pix.distance2line(*p[2],*p[1]);
  d[2] = pix.distance2line(*p[0],*p[1]);

  w[0] *= errorDistWeighting(p[2]->dist(*p[0]),delta);
  w[1] *= errorDistWeighting(p[2]->dist(*p[1]),delta);
  w[2] *= errorDistWeighting(p[0]->dist(*p[1]),delta);

  certainty = 1;
  if (d[0]<1) return w[0];						//point on (p,p1): return w1
  if (d[1]<1) return w[1];						//point on (p,p2): return w2
  if (d[2]<1) return w[2];						//point on (p,p2): return w2

  float B   = blendDistance(pix,tr);
  certainty = 1-B;								//Record the certainty (0 for deepest point in triangle, 1 for being on the edges)

  float l[3],A[3];								//Use barycentric coordinates to interpolate
  l[0] = p[0]->dist(*p[2]);
  l[1] = p[1]->dist(*p[2]);
  l[2] = p[0]->dist(*p[1]);
  A[0] = 1/(d[0]*l[0]);
  A[1] = 1/(d[1]*l[1]);
  A[2] = 1/(d[2]*l[2]);

  float val = (w[0]*A[0]+w[1]*A[1]+w[2]*A[2])/(A[0]+A[1]+A[2]);

  return val;
}


float PointCloud::averageNeighborDist(int pid) const							//Return average dist to geometric nbs of point 'pid'
{
  const Point2d& pi = points[pid];
  const EdgeMatrix::Row& erow = (*sorted_edges)(pid);
  float avgdist = 0;

  for(EdgeMatrix::Row::const_iterator it = erow.begin();it!=erow.end();++it)
    avgdist += pi.dist(points[it->pid]);
  avgdist /= erow.size();
  return avgdist;
}

//float euclidean_distance(vector<double> v1, vector<double> v2) {
//	double dist = 0.0f, d_aux;
//	if (v1.size() == v2.size()) {
//		for (int i = 0; i < v1.size(); ++i) {
//			d_aux = v1[i] - v2[i];
//			dist += d_aux * d_aux;
//		}
//	} else {
//		cout << "ERROR: trying to calculate Euclidean distance between vectors with different size." << endl;
//		exit(-1);
//	}
//	return sqrt(dist);
//}

// error matrix (wrt projection of n-D points to 2-D points)
bool PointCloud::loadDistanceErrors(string dataset, string projection) {
	string filename = dataset + "." + projection + ".err";
	FILE *fp = fopen(filename.c_str(), "r");
	int NP = points.size();
	int ND = attributes.size();

	distmatrix = new DistMatrix(NP);

	// First time loading a specific dataset+projection pair, we need to calculate the errors
	if (!fp) {
		// For some reason we don't have the nD points cached anywhere in proper form.
		// The original data is currently stored "attribute-first", so...
		vector<vector<double>> nd_points(NP);
		for (int i = 0; i < NP; i++)
			for (int j = 0; j < ND; j++)
				nd_points[i].push_back((*attributes[j])[i]);

		// 2D euclidean distance, all-to-all, normalized
		vector<vector<double>> dist_2d(NP);
		double max_2d_dist = 0.0f;
		for (int i = 0; i < NP; ++i) {
			dist_2d[i].resize(NP);
			dist_2d[i][i] = 0.0f;
			for (int j = 0; j < NP; ++j) {
				if (i != j) {
					dist_2d[i][j] = points[i].dist(points[j]);
					max_2d_dist = max(max_2d_dist, dist_2d[i][j]);
				}
			}
		}
		for (int i = 0; i < NP; ++i)
			for (int j = 0; j < NP; ++j)
				dist_2d[i][j] /= max_2d_dist;

		// nD euclidean distance, all-to-all, normalized
		vector<double> norm(NP, 0.0);
		for (int i = 0; i < NP; ++i) {
			for (int j = 0; j < ND; j++) {
				norm[i] += nd_points[i][j] * nd_points[i][j];
			}
			norm[i] = sqrt(norm[i]);
		}
		vector<vector<double>> dist_nd(NP);
		double max_nd_dist = 0.0f;
		for (int i = 0; i < NP; ++i) {
			dist_nd[i].resize(NP);
			dist_nd[i][i] = 0.0f;
			for (int j = 0; j < NP; ++j) {
				if (i != j) {
					double dot = 0.0;
					for (int k = 0; k < ND; k++)
						dot += nd_points[i][k] * nd_points[j][k];
					dist_nd[i][j] = sqrt(abs(norm[i] * norm[i] + norm[j] * norm[j] - 2 * dot));
					//dist_nd[i][j] = euclidean_distance(nd_points[i], nd_points[j]);
					max_nd_dist = max(max_nd_dist, dist_nd[i][j]);
				}
			}
		}
		for (int i = 0; i < NP; ++i)
			for (int j = 0; j < NP; ++j)
				dist_nd[i][j] /= max_nd_dist;

		// Third step: simply derive the error, all-to-all
		for (int i = 0; i < NP; ++i)
			for (int j = 0; j < NP; ++j)
				(*distmatrix)(i,j) = (float)(dist_2d[i][j] - dist_nd[i][j]);

		// Now write a file so that we don't have to calculate this again
		fp = fopen(filename.c_str(), "w");
		fprintf(fp, "%d\n", NP);
		for (int i = 0; i < NP; ++i) {
			for (int j = 0; j < i; ++j)
				fprintf(fp, "%lf;", (*distmatrix)(i,j));
			fprintf(fp, "%lf\n", 0.0);
		}

		return true;
	} else {

		int nrows;
		fscanf(fp,"%d",&nrows);
		if (nrows!=NP)
		{
			cout<<"Error: error-matrix #rows "<<nrows<<" != #points "<<NP<<endl;
			return false;
		}

		for(int i=0;i<NP;++i) {
			for(int j=0;j<=i;++j)
			{
				float val;
				if (j<i) fscanf(fp,"%f;",&val);								//Take care, last elem on line not followed by ';'
				else														//Also, note that the matrix stored in the file is symmatric lower-diagonal
				{															//REMARK: We could store this more compactly..
					fscanf(fp,"%f",&val);
					continue;
				}

				(*distmatrix)(i,j) = val;
				(*distmatrix)(j,i) = val;
			}
		}
		fclose(fp);

		distmatrix->minmax();											//3. Compute range of distance matrix
		//   -the range [min,0]: false positives (points too close in 2D w.r.t. nD)
		//   -the range [0,max]: false negatives (points too far in 2D w.r.t. nD)
		cout<<"Error matrix: ["<<distmatrix->min()<<","<<distmatrix->max()<<"]"<<endl;

		return true;
	}
	return false;
}
