#include <cstdlib>

#include "include/scalarimage.h"
#include "include/pointcloud.h"
#include "include/sparsematrix.h"
#include "include/vis.h"

#include "include/Shader.h"
#include "include/Program.h"

float ImageInterpolator::rad_blur = 5.0f;
ImageInterpolator *ImageInterpolator::instance = nullptr;

ImageInterpolator::ImageInterpolator(const PointCloud *cloud): cloud(cloud) {

	int size = cloud->fboSize;

	// Shaders and programs
	std::vector<tdogl::Shader> shaders1;
	shaders1.push_back(tdogl::Shader::shaderFromFile("src/shader1.vert", GL_VERTEX_SHADER));
	shaders1.push_back(tdogl::Shader::shaderFromFile("src/shader1.frag", GL_FRAGMENT_SHADER));
	gProgram[0] = new tdogl::Program(shaders1);
	std::vector<tdogl::Shader> shaders2;
	shaders2.push_back(tdogl::Shader::shaderFromFile("src/shader2.vert", GL_VERTEX_SHADER));
	shaders2.push_back(tdogl::Shader::shaderFromFile("src/shader2.frag", GL_FRAGMENT_SHADER));
	gProgram[1] = new tdogl::Program(shaders2);

	// Vertex Buffers and Arrays
	glGenVertexArrays(2, gVAO);
	glGenBuffers(3, gVBO);
	// Array #1: projection points (and corresponding scalars)
	glBindVertexArray(gVAO[0]);
	// Array #1, Buffer #1: point coordinates
	GLfloat *vert_buffer = new GLfloat[cloud->size()*2];
	for (int i = 0; i < cloud->size(); ++i) {
		vert_buffer[2*i] = cloud->points[i].x;
		vert_buffer[2*i+1] = cloud->points[i].y;
	}
	glBindBuffer(GL_ARRAY_BUFFER, gVBO[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*cloud->size()*2, vert_buffer, GL_STATIC_DRAW);
	delete vert_buffer;
	glEnableVertexAttribArray(gProgram[0]->attrib("vert"));
	glVertexAttribPointer(gProgram[0]->attrib("vert"), 2, GL_FLOAT, GL_FALSE, 0, 0);
	// Array #1, Buffer #2: point scalars (will be updated each draw, for now)
	glBindBuffer(GL_ARRAY_BUFFER, gVBO[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*cloud->size(), NULL, GL_STREAM_DRAW);
	glEnableVertexAttribArray(gProgram[0]->attrib("scalar"));
	glVertexAttribPointer(gProgram[0]->attrib("scalar"), 1, GL_FLOAT, GL_FALSE, 0, 0);

	// Array #2: texture quad
	glBindVertexArray(gVAO[1]);
	// Array #2, Buffer #3: quad vertices
	GLfloat vert_buffer2[] = { -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f };
	glBindBuffer(GL_ARRAY_BUFFER, gVBO[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vert_buffer2), vert_buffer2, GL_STATIC_DRAW);
	glEnableVertexAttribArray(gProgram[1]->attrib("vert"));
	glVertexAttribPointer(gProgram[1]->attrib("vert"), 2, GL_FLOAT, GL_FALSE, 0, 0);

	GLfloat *tmp = new GLfloat[size*size*2];
	for (int i = 0; i < size*size*2; i+=2) {
		tmp[i] = cloud->siteDT[i/2];
		tmp[i+1] = 0.0f;
	}
	// Textures
	glGenTextures(3, gTexture);
	// Texture that will store parameters
	glBindTexture(GL_TEXTURE_2D, gTexture[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, size, size, 0, GL_RG, GL_FLOAT, tmp);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	// Intermediate texture that will be drawn in pass 1 and read in pass 2
	glBindTexture(GL_TEXTURE_2D, gTexture[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, size, size, 0, GL_RGBA, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	// Final texture that will be drawn in pass 2
	/*
	glBindTexture(GL_TEXTURE_2D, gTexture[2]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, size, size, 0, GL_RGBA, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	*/

	// Framebuffer
	glGenFramebuffers(1, &gFBO);

	// just abort everything if it fails
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "Error in FBO setup." << std::endl;
		exit(-1);
	}

	/*
	glBindFramebuffer(GL_FRAMEBUFFER, gFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gTexture[0], 0);
	GLfloat* data = new GLfloat[size*size*4];
	glReadPixels(0, 0, size, size, GL_RGBA, GL_FLOAT, data);
	for (int i = 0, offs = 0; i < size; ++i)
		for (int j = 0; j < size; ++j, ++offs) {
			fprintf(stderr, "%f %f %f %f -- %f\n", data[i*size+4*j], data[i*size+4*j+1], data[i*size+4*j+2], data[i*size+4*j+3], cloud.siteDT[offs]);
		}
	*/

	// clean up and go away
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

ImageInterpolator::~ImageInterpolator() {
	for (int i = 0; i < 2; ++i)
		delete gProgram[i];
	glDeleteTextures(3, gTexture);
	glDeleteFramebuffers(1, &gFBO);
	glDeleteVertexArrays(2, gVAO);
	glDeleteBuffers(2, gVBO);
}

void ImageInterpolator::shepard(const float* point_data, const GLuint out_tex) {

	float rad_max = cloud->avgdist;
	int size = cloud->fboSize;

	// TODO: This shouldn't really have to happen everytime... :(
	glBindBuffer(GL_ARRAY_BUFFER, gVBO[1]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat)*cloud->size(), point_data);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// These are used on both passes
	glEnable(GL_BLEND);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glEnable(GL_POINT_SPRITE);
	glBindFramebuffer(GL_FRAMEBUFFER, gFBO);

	// ***** FIRST PASS *****

	// Additive blending; everything will be accumulated in the first pass
	glBlendFunc(GL_ONE, GL_ONE);

	// Setup 1st program
	glUseProgram(gProgram[0]->object());
	gProgram[0]->setUniform("rad_max", rad_max);
	gProgram[0]->setUniform("rad_blur", rad_blur);

	// The first texture carries 1 value into the shader for reading:
	//   [0] The distance to the nearest point (siteDT) for every pixel.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gTexture[0]);
	gProgram[0]->setUniform("siteDT", 0);
	// dont unbind! this texture is used in the shader

	// Draw to FBO (intermediate texture)
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gTexture[1], 0);
	glClearColor(1,1,1,1); // white
	glClear(GL_COLOR_BUFFER_BIT);
	glBindVertexArray(gVAO[0]);
	glDrawArrays(GL_POINTS, 0, cloud->size());
	glBindVertexArray(0);

	// ************* SECOND PASS

	// This time we add the transparency accordingly
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Setup 2nd program
	glUseProgram(gProgram[1]->object());
	gProgram[1]->setUniform("rad_max", rad_max);

	// setup input textures
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gTexture[0]);
	gProgram[1]->setUniform("siteDT", 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gTexture[1]);
	gProgram[1]->setUniform("accumTex", 1);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_1D, Display::tex_colormap);
	gProgram[1]->setUniform("colormap", 2);

	// Draw to FBO (final texture)
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, out_tex, 0);
	glClearColor(1, 1, 1, 1); // white
	glClear(GL_COLOR_BUFFER_BIT);
	glBindVertexArray(gVAO[1]);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);

	// ***** SETUP THE RETURN IMAGE
	//glReadPixels(0, 0, size, size, GL_RGBA, GL_FLOAT, out_image);

	// ***** CLEAN UP AND GO AWAY
	glUseProgram(0);
	glDisable(GL_BLEND);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	/*
	for (int i = 0, offs = 0; i < size; ++i)
		for (int j = 0; j < size; ++j, ++offs) {
				out_image[4*offs+0] = rand() / (float) RAND_MAX;
				out_image[4*offs+1] = rand() / (float) RAND_MAX;
				out_image[4*offs+2] = rand() / (float) RAND_MAX;
				out_image[4*offs+3] = 1.0f;
			}
	*/
}


ScalarImage::ScalarImage(int wd,int ht): image_max(0)
{
	image = new FIELD<float>(wd,ht);
	*image = 0;

	certainty = new FIELD<float>(wd,ht);
	*certainty = 0;
}




ScalarImage::~ScalarImage()
{
	delete image;
	delete certainty;
}




void ScalarImage::interpolateDistMatrix(const PointCloud& pc,float delta)
{
  image_max = 0;
  for(int i=0,Y=image->dimY();i<Y;++i)
  {
    for(int j=0,X=image->dimX();j<X;++j)
    {
      Point2d pix((float)j,(float)i);									//current point to interpolate to
      float cert;
      float val = pc.interpolateDistMatrix(pix,cert,delta);
      image->value(j,i) = val;
      if (image_max<val) image_max = val;
      certainty->value(j,i) = cert;
    }
  }
}
