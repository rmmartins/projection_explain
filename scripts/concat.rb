#!/usr/bin/ruby

if ARGV.size < 3
	puts "Expected 3 arguments: dataset proj1 proj2"
	exit
end

dataset = ARGV[0]
proj1 = ARGV[1]
proj2 = ARGV[2]

file1 = File.new("#{dataset}.#{proj1}.2d")
file2 = File.new("#{dataset}.#{proj2}.2d")

file1.gets # DY
file2.gets # DY

lines1 = file1.gets.to_i
lines2 = file2.gets.to_i

if lines1 != lines2
	puts "Files have different number of elements, that doesn't make sense."
	exit
end

file1.gets # cols = 2
file2.gets # cols = 2

file1.gets # labels
file2.gets # labels

outfile1 = File.new("#{dataset}.#{proj1}.#{proj2}.2d", "w")

outfile1.puts "DY"
outfile1.puts lines1 + lines2
outfile1.puts 2
outfile1.puts ""

elems1 = Array.new
elems2 = Array.new

(0...lines1).each do |i|
	line = file1.gets.split(";")
	line[0] = "1#{i}"
	elems1.push Array.new({line[0].to_i, line[1].to_f, line[2].to_f, line[3].to_f})
	outfile1.puts line.join(";")
end

(0...lines2).each do |i|
	line = file2.gets.split(";")
	line[0] = "2#{i}"
	elems2.push Array.new(line[0].to_i, line[1].to_f, line[2].to_f, line[3].to_f)
	outfile1.puts line.join(";")
end

outfile1.close

outfile2 = File.new("#{dataset}.#{proj1}.#{proj2}.err", "w")

outfile2.puts lines1 + lines2

# right now i'm interested in showing corresponding groups between the two projections
(0...lines1).each do |i|
	(0..i).each do |j|
		outfile2.printf "%.8f ", Math.hypot(elems1[j][1]-elems1[i][1],elems1[j][2]-elems1[i][2])
	end
	outfile2.puts
end

outfile2.close		

