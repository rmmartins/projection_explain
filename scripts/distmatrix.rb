#!/usr/bin/ruby -w

# Parameters
name = ARGV[0]
proj = ARGV[1]

if not name or not proj
	puts "Usage: distmatrix.rb <name> <proj>"
	exit
end

fileNd = File.open("#{name}.nd", 'r')
fileNdDmat = File.open("#{name}.nd.dmat", 'w')
file2d = File.open("#{name}.#{proj}.2d", 'r')
file2dDmat = File.open("#{name}.#{proj}.dmat", 'w')

# Ignore first line ("DY")
fileNd.gets
file2d.gets

# Check element count
count = fileNd.gets.to_i
count2 = file2d.gets.to_i

if count != count2
	puts "ERROR: Different number of elements in nD and 2D."
	exit
end

fileNdDmat.write "#{count}\n"
file2dDmat.write "#{count}\n"

# Number of dimensions important for nd, ignore for 2d (always 2, of course)
nd = fileNd.gets.to_i
file2d.gets

# Ignore dimensions labels
fileNd.gets
file2d.gets

# Get the points

nDpoints = Array.new
tDpoints = Array.new

(0...count).each do
	pNd = fileNd.gets.split(';').collect{|e| e.to_f}
	pNd.delete_at(pNd.size-1)
	pNd.delete_at(0)
	nDpoints << pNd
	p2d = file2d.gets.split(';').collect{|e| e.to_f}
	p2d.delete_at(p2d.size-1)
	p2d.delete_at(0)
	tDpoints << p2d
end

# Generate distance matrices

def euclidean_dist(p1, p2, n)
	sum = 0
	(0...n).each { |i| sum += ((p1[i] - p2[i])**2) }
	Math.sqrt(sum)
end

(0...count).each do |i|
	(0..i).each do |j|
		fileNdDmat.printf "%.8f ", euclidean_dist(nDpoints[i], nDpoints[j], nd)
		file2dDmat.printf "%.8f ", euclidean_dist(tDpoints[i], tDpoints[j], 2)
	end
	fileNdDmat.puts
	file2dDmat.puts
end

fileNd.close
fileNdDmat.close
file2d.close
file2dDmat.close
