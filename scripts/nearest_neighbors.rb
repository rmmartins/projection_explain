#!/usr/bin/ruby -w

# Parameters
name = ARGV[0]
proj = ARGV[1]

if not name or not proj
	puts "Usage: nearest_neighbors.rb <name> <proj>"
	exit
end

fileNd = File.open("#{name}.nd.dmat", 'r')
fileNdNN = File.open("#{name}.nd.nn", 'w')
fileNdErr = File.open("#{name}.nd.nn.err", "w")
file2d = File.open("#{name}.#{proj}.dmat", 'r')
file2dNN = File.open("#{name}.#{proj}.nn", 'w')
file2dErr = File.open("#{name}.#{proj}.nn.err", "w")

# Check element count
count = fileNd.gets.to_i
count2 = file2d.gets.to_i

if count != count2
	puts "ERROR: Different number of elements in nD and 2D."
	exit
end

fileNdNN.write "#{count}\n"
file2dNN.write "#{count}\n"

# Read distance matrices

dmatNd = Array.new(count){Array.new}
dmat2d = Array.new(count){Array.new}

(0...count).each do |i|
	j = 0
	fileNd.gets.split.each do |dist|
		dmatNd[i] << [j, dist]
		dmatNd[j] << [i, dist] if i != j
		j = j + 1
	end
	j = 0
	file2d.gets.split.each do |dist|
		dmat2d[i] << [j, dist]
		dmat2d[j] << [i, dist] if i != j
		j = j + 1
	end
end

(0...count).each do |i|
	dmatNd[i].sort! { |a,b| a[1] <=> b[1] }
	dmatNd[i].collect! { |a| a[0] }
	fileNdNN.puts dmatNd[i].join(" ")
	dmat2d[i].sort! { |a,b| a[1] <=> b[1] }
	dmat2d[i].collect! { |a| a[0] }
	file2dNN.puts dmat2d[i].join(" ")
end

(0...count).each do |i|
	(1..dmatNd[i].size).each do |setsize|
		if (dmatNd[i].slice(0,setsize) == dmat2d[i].slice(0,setsize))


end


fileNd.close
fileNdNN.close
file2d.close
file2dNN.close
