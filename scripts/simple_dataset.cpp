#include <cstdlib>
#include <cstdio>
#include <ctime>

using namespace std;

#define NUM_DIMS 3
#define NUM_PTS 1000

int group_size = NUM_PTS / NUM_DIMS;

double data[NUM_PTS][NUM_DIMS];
double cdata[NUM_PTS];

void print_data();
void print_axis();

int main() {
	srand(time(NULL));	
	for (int i = 0; i < NUM_PTS; ++i) {
		cdata[i] = (double) (i / group_size);
		for (int j = 0; j < NUM_DIMS; ++j) {
			double val = ((double) rand()) / RAND_MAX;
			if (j == cdata[i]) 
				data[i][j] = val * 0.1;
			else
				data[i][j] = val * val;
		}
	}	
	print_data();
	print_axis();
	return 0;
}

void print_data() {
	FILE *f = fopen("simple3d.data","w");
	fprintf(f, "DY\n");
	fprintf(f, "%d\n", NUM_PTS);
	fprintf(f, "%d\n", NUM_DIMS);
	for (int i = 0; i < NUM_DIMS-1; ++i)
		fprintf(f, "dim_%d;", i);
	fprintf(f, "dim_%d\n", NUM_DIMS-1);
	for (int i = 0; i < NUM_PTS; ++i) {
		fprintf(f, "point_%d;", i);
		for (int j = 0; j < NUM_DIMS; ++j)
			fprintf(f, "%.8lf;", data[i][j]);
		fprintf(f, "%.1lf\n", cdata[i]);
	}
	fclose(f);
}

void print_axis() {
	FILE *f = fopen("simple3d.axis.data","w");
	fprintf(f, "DY\n");
	fprintf(f, "%d\n", NUM_DIMS * 100);
	fprintf(f, "%d\n", NUM_DIMS);
	fprintf(f, "\n");
	for (int i = 0; i < NUM_DIMS; ++i) {
		for (int j = 0; j < 100; ++j) {
			fprintf(f, "dim_%d;", i);
			for (int k = 0; k < NUM_DIMS; ++k) {
				if (k == i)
					fprintf(f, "%lf;", 0.0);
				else
					fprintf(f, "%lf;", ((double) j) / 100);
			}
			fprintf(f, "%lf\n", 0.0);
		}
	}
	fclose(f);
}