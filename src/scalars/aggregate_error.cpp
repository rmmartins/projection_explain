#ifdef PLATFORM_WIN
#define NOMINMAX
#include <algorithm>
#endif

#include "scalar.h"
#include "pointcloud.h"
#include "fullmatrix.h"

//Compute aggregated projection error for a point wrt all its neighbors
void AggregateError::compute(float norm, int pid)
{
  const int NP = cloud->size();

  for(int i=0;i<NP;++i)
  {
    const PointCloud::DistMatrix::Row& row = (*(cloud->distmatrix))(i);
    _values[i] = 0.0f;
    for(auto it=row.begin();it!=row.end();++it)
    {
      _values[i] += fabs(*it) / (float)NP;
    }
  }

  normalize(norm);
  updateImage();
}
