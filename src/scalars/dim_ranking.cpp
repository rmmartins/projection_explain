#include <algorithm>
#include <numeric>
#include "ANN/ANN.h"
#include "scalar.h"
#include "pointcloud.h"
//#include "include/point2d.h"


static DimRanking *_instance;
static int selected_pid;
static GLUI* _glui;

static vector<double> global_attr_dispersion;

enum
{
  STD_DEV,
  CONTRIB
};


// Sample Standard Deviation / Variance
static double variance(const std::vector<double>& v) {
  double sum = std::accumulate(std::begin(v), std::end(v), 0.0);
  double m =  sum / v.size();

  double accum = 0.0;
  std::for_each (std::begin(v), std::end(v), [&](const double d) {
    accum += (d - m) * (d - m);
  });

  //return sqrt(accum / (v.size()-1));
  return accum / (v.size()-1);
}

static double contrib(const std::vector<double>& v, const std::vector<ANNidx>& p, const PointCloud* cloud) {
  // v[0] == value at center point
  double sum = 0.0;
  Point2d p0 = cloud->points[p[0]];
  for (unsigned int j = 1; j < p.size(); ++j) {
    Point2d pj = cloud->points[p[j]];
    sum += (1.0 - abs(v[0] - v[j])) / p0.dist(pj);
  }
  return sum;
}

DimRanking::DimRanking(const PointCloud* pc) : Scalar(pc, "Dimension Ranking") {
  // default/initial values
  k = (int)sqrt((double)pc->size());
  // global dispersion of each variable
  global_attr_dispersion.resize(pc->attributes.size());
  for (unsigned int i = 0; i < pc->attributes.size(); ++i) {
    vector<double> v((*pc->attributes[i]).begin(),(*pc->attributes[i]).end());
    global_attr_dispersion[i] = variance(v);
  }
  compute();
}

void DimRanking::compute(float norm_range, int pid)
{
  selected_pid = pid;
  // const reference to the pointcloud's 2D neighborhood cache (just a shortcut)
  const vector<ANNidxArray>& neigh_2d = cloud->neighborhood_matrix_2d;
  // reusable vector, will contain the point ids of each processed neighborhood (order: ascending distance from center)
  vector<ANNidx> point_neighborhood(k+1);
  // reusable vector, will contain the values of a dimension in each point of a neighborhood (in the same order as
  // point_neighborhood)
  vector<double> neigh_dim_vec(k+1, 0.0);
  // run through the whole projection, centering on each point at a time
  for (int center_pid = 0; center_pid < cloud->size(); ++center_pid) {
    _values[center_pid] = 0.0f;
    // do the processing if there are no selected points (all) or if we are currently on the selected point
    if (pid == -1 || center_pid == pid) {
      //std::cout << "center pid: " << center_pid << std::endl;
      //std::cout << "neighborhood:" << std::endl;
      // get the k-nearest-neighbors of the center (including itself)
      for (int j = 0; j <= k; ++j) {
        point_neighborhood[j] = neigh_2d[center_pid][j];
        //std::cout << neigh_2d[center_pid][j] << " ";
      }
      //std::cout << std::endl;
      double min_weight = DBL_MAX;
      // process each dimension inside the neighborhood
      for (int dim = 0; dim < cloud->dimensions(); ++dim) {
        // for each point q in the neighborhood
        for (int q = 0; q <= k; ++q) {
          // get the normalized value of dim in q
          neigh_dim_vec[q] = (*cloud->attributes[dim])[point_neighborhood[q]] - cloud->attributes_min[dim];
          neigh_dim_vec[q] /= cloud->attributes_max[dim] - cloud->attributes_min[dim];
        }
        // get the weight of dim according to the current metric
        double weight = 0.0;
        switch (metric) {
          case STD_DEV:
            weight = variance(neigh_dim_vec) / global_attr_dispersion[dim];
            break;
          case CONTRIB:
            weight = 1.0 - contrib(neigh_dim_vec, point_neighborhood, cloud);
            break;
        }
        //std::cout << "dim: " << dim << ", weight: " << weight << std::endl;
        if (weight < min_weight) {
          min_weight = weight;
          _values[center_pid] = (float)dim;
        }
      }
    }
  }
  normalize(norm_range);
  updateImage();
}

static void control_cb(int ctrl) {
  _instance->compute(0.0f, selected_pid);
  _glui->post_update_main_gfx();						//Update GLUT window upon any parameter change (i.e., redraw)
}

void DimRanking::options(GLUI* glui) {
  _instance = this;
  _glui = glui;
  GLUI_Rollout* rollout = glui->add_rollout(name.c_str(), false);
  GLUI_RadioGroup *metric_grp = new GLUI_RadioGroup(rollout, &metric, 0, control_cb);
  new GLUI_RadioButton(metric_grp, "Variance");
  new GLUI_RadioButton(metric_grp, "Contribution");
}
