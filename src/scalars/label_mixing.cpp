#include "scalar.h"
#include "pointcloud.h"

//Compute smooth label-mixing image 'tex_mixing' from the per-point label-mixing metric stored in the cloud.
void LabelMixing::compute (float norm, int pid) {
  //Compute per-point label-mix degree (in [0,1]). This tells from how many of the
  //point-neighbor-labels (in its triangle-fan) do the point's label differ.
  for(int i = 0; i < cloud->size(); ++i) {
    const Point2d& pi = cloud->points[i];                      //Compute label-mixing at point 'pi':
    float lbl_i = cloud->class_data[i];

    std::vector<int> nn;                             //Get R-nearest neighbors, and see how much mixing we've got in there
    const float rad = cloud->avgdist;
    int NN = cloud->searchR(pi, rad, 200, nn);

    float wsum = 0;                             //Compute weights for all neighbors
    float diff = 0;
    for(int j = 0; j < NN; ++j) {
      float r  = pi.dist(cloud->points[nn[j]]);
      float w  = exp(-r*r/rad/rad);
      wsum    += w;

      float lbl_j = cloud->class_data[nn[j]];
      if (lbl_j == lbl_i) continue;
      diff += w;
    }

    _values[i] = diff/wsum;
  }

  updateImage();

}
