#ifdef PLATFORM_WIN
#define NOMINMAX
#include <algorithm>
#endif

#include <vector>
#include "scalar.h"
#include "pointcloud.h"
#include "fullmatrix.h"

// Compute false-negatives w.r.t. point 'pid'
void MissingNeighbors::compute(float norm_range, int pid)
{
  // 'row' encodes errors of all points w.r.t. 'pid'
  const PointCloud::DistMatrix::Row& row = (*(cloud->distmatrix))(pid);

  unsigned int idx = 0;
  for(auto it = row.begin(); it != row.end(); ++it, ++idx)
  {
    // We only want to emphasize false negatives here, so we skip false positive points.
    _values[idx] = std::max(*it, 0.0f);
  }

/*
  float err_sum = 0;															//Assign relative error for 'pid' (wrt itself..)
  const PointCloud::EdgeMatrix::Row& erow = (*(cloud->sorted_edges))(pid);							//as being the average error wrt its neighbors
  for(auto it = erow.begin();it!=erow.end();++it)
  {
    int     j = it->pid;
    err_sum += _values[j];
  }

  _values[pid] = err_sum/erow.size();
*/

  normalize(norm_range);
  updateImage();
}

void MissingNeighbors::compute(float range, const Grouping::PointGroup& grp)
{																				//Compute false-negatives w.r.t. all points in 'grp'
  const int NP = cloud->size();
  std::vector<float> rerr(NP,1.0e+6);

  for(Grouping::PointGroup::const_iterator it=grp.begin();it!=grp.end();++it)
  {
    int pid = *it;
    compute(0.0f, pid);										//Compute FN-error of all points wrt 'pid'
    for(int i=0;i<NP;++i)
      rerr[i] = std::min(rerr[i],_values[i]);
  }

  if (!range)																	//Auto-normalization
  {
    for(int i=0;i<NP;++i) range = std::max(range,rerr[i]);
    if (range<1.0e-6) range = 1;
  }

  for(int i=0;i<NP;++i) _values[i] = std::min(rerr[i]/range,1.0f);

  updateImage();
}
