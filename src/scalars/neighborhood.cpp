#ifdef PLATFORM_WIN
#define NOMINMAX
#include <algorithm>
#endif

#include <vector>
#include <cmath>
#include <numeric>

#include "ANN/ANN.h"
#include "scalar.h"
#include "pointcloud.h"
#include "fullmatrix.h"
#include "include/graph.h"
#include "include/gdrawingcloud.h"

// File scope; because... why not?
static Neighborhood *_instance;
static int selected_pid;
static GLUI* _glui;
static Graph* neighborhood_graph = 0;    // Graph linking a point with its nearest neighbors (in nD or 2D, user-defined)
static vector<float> edge_values;
static int diss_type;

enum
{
	UI_NEIGH_KNN_TEX,
	UI_NEIGH_KNN_EDG,
	UI_NEIGH_TEX_D,
	UI_NEIGH_EDG_D,
	UI_NEIGH_BUNDLES,
	UI_NEIGH_SYNC_K,
	UI_NEIGH_nearest_first_COLORS,
	UI_NEIGH_TEX_EXCL,
	UI_NEIGH_EDG_EXCL,
	UI_DISS
};

// For now it uses the same values as the texture; needs a second scalar vector (like _values)
// The values are calculated in "compute", so this has to be called after that
void Neighborhood::computeEdges()
{
  // For now, only showing results for single selection
  if (selected_pid > -1) {
    // 1. Construct the graph (as a sparse adj-matrix)
    delete neighborhood_graph;
    neighborhood_graph = new Graph(cloud->size());
    int edge_count = 0;
    for (int i = 0; i < cloud->size(); ++i)
      if (edge_values[i] > 0.0f) {
        (*neighborhood_graph)(std::min(selected_pid, i), std::max(selected_pid, i)) = edge_values[i];
        edge_count++;
      }
    // 2. Construct a graph drawing for 'neighborhood_graph'
    delete bundling;
    bundling = new GraphDrawingCloud();
    bundling->build(neighborhood_graph, cloud);
  }
}


Neighborhood::Neighborhood(const PointCloud* pc) : Scalar(pc, "Neighborhood")
{
	// default/initial values
	tex_k = edg_k = (int)sqrt((double)pc->size());
	tex_nd = edg_nd = 0;
	tex_excl = edg_excl = false;
	tex_nearest_first = edg_nearest_first = 1;
	draw_edges = 0;
	bundling = nullptr;
	edge_values.resize(pc->size());
	compute();
}


void computeValues(vector<float>& _values, const PointCloud* cloud, int nd, int k, bool nearest_first, bool excl) {
	int N = cloud->size();

	//if (nd == 2) { // true neighbors
	//	if (pid > -1) {
	//		const std::vector<ANNidxArray>& matrix_2d = cloud->neighborhood_matrix_2d;
	//		const std::vector<ANNidxArray>& matrix_nd = cloud->neighborhood_matrix_nd;
	//		for (int i = 0; i < N; ++i)
	//			_values[i] = 0.0f;
	//		std::vector<float> aux(N, 0.0f);
	//		// start with 2d neighbors
	//		for (int j = 0; j <= k; ++j)
	//			aux[matrix_2d[pid][j]] = nearest_first ? (float) k - j + 1 : (float) j;
	//		// then compare with nd neighbors
	//		for (int j = 0; j <= k; ++j) {
	//			if (aux[matrix_nd[pid][j]] > 0.0f) {
	//				float val = (float)j+aux[matrix_nd[pid][j]];
	//				_values[matrix_nd[pid][j]] = nearest_first ? 2*k - val + 1 : val;
	//			}
	//		}
	//	}
	//}
	// If the user has selected True Neighbors
	if (nd == 2) {
		const std::vector<ANNidxArray>& matrix_2d = cloud->neighborhood_matrix_2d;
		const std::vector<ANNidxArray>& matrix_nd = cloud->neighborhood_matrix_nd;
		// If there is a selected point
    if (selected_pid > -1) {
			// We start by finding out all i_2d's and i_nd's
      vector<int> i_2d(N, 0), i_nd(N, 0);
			for (int i = 0; i < N; ++i) {
				i_2d[matrix_2d[selected_pid][i]] = i;
				i_nd[matrix_nd[selected_pid][i]] = i;
			}
			int union_count = 0, inters_count = 0;
			// Now we can just implement the formula
			for (int i = 0; i < N; ++i) {
				_values[i] = 0;
				switch(diss_type) {
				case 0: // Jaccard
					if (i_2d[i] <= k || i_nd[i] <= k) {
						union_count++;
						inters_count += (i_2d[i] <= k && i_nd[i] <= k);
						_values[i] = 1.0f - (i_2d[i] <= k && i_nd[i] <= k);
					}
					break;
				case 1: // Custom k+1
					if (i_2d[i] > k)
						i_2d[i] = k+1;
					if (i_nd[i] > k)
						i_nd[i] = k+1;
					// intentional fall-through
				case 2: // Custom True Rank
					if (i_2d[i] <= k)
						_values[i] += ((k+1)-i_2d[i])*abs(i_2d[i]-i_nd[i]);
					if (i_nd[i] <= k)
						_values[i] += ((k+1)-i_nd[i])*abs(i_2d[i]-i_nd[i]);
					break;
				}
			}
			cout << "Union count: " << union_count << endl;
			cout << "Inters. count: " << inters_count << endl;
    }
		// If there are no selected points, we show the aggregate preservation error on all points
		else {
			for (int i = 0; i < N; ++i) {
				_values[i] = 0.0f;
			}
			double max_error = 0.0f;
			// We'll compute the dissimilarity centered on every point i
			for (int i = 0; i < N; ++i) {
				// i_2d and i_nd are the 2d and nd k-neighborhoods, respectively
				vector<int> i_2d(N, 0), i_nd(N, 0);
				// Build and compare both neighborhoods
				for (int j = 0; j < N; ++j) {
					i_2d[matrix_2d[i][j]] = j;
					i_nd[matrix_nd[i][j]] = j;
				}
				int unio = 0, inters = 0;
				switch(diss_type) {
				case 0: // Jaccard
					for (int j = 0; j < N; ++j) {
						unio += (i_2d[j] <= k || i_nd[j] <= k);
						inters += (i_2d[j] <= k && i_nd[j] <= k);
						/*if (i_2d[j] <= k || i_nd[j] <= k)
							_values[j] += (i_2d[j] <= k && i_nd[j] <= k);*/
					}
					_values[i] = 1.0f - (unio == 0 ? 1 : ((float)inters/(float)unio));
					break;
				case 1: // Custom k+1
					for (int j = 0; j < N; ++j) {
						if (i_2d[j] > k)
							i_2d[j] = k+1;
						if (i_nd[j] > k)
							i_nd[j] = k+1;
					}
					// Intentional fall-through
				case 2: // Custom True Rank
					// 2D sum
					for (int j = 1; j <= k; ++j) {
						int id = matrix_2d[i][j];
						_values[i] += ((k+1)-i_2d[id])*abs(i_2d[id]-i_nd[id]);
					}
					// nD sum
					for (int j = 1; j <= k; ++j) {
						int id = matrix_nd[i][j];
						_values[i] += ((k+1)-i_nd[id])*abs(i_2d[id]-i_nd[id]);
					}
					break;
				case 3:
					// Schreck 2010
					int ND = cloud->attributes.size();
					// I have to search k+1 because the first neighbor is always the query point itself
					ANNdistArray dists_nd = new ANNdist[k+1];
					ANNidxArray nn_idx = new ANNidx[k+1];
					ANNpoint query_pt = annAllocPt(ND);
					for (int j = 0; j < ND; j++)
						query_pt[j] = (*(cloud->attributes)[j])[i];
					ANNtree *tree = cloud->kdtn;
					tree->annkSearch(query_pt, k+1, nn_idx, dists_nd);
					// On the paper, "nn_idx" is "i_{o,0},...,i_{o,n}" and "dists_nd" is "d^O_{o,n}"
					// Now we build dists_2d = "d^P_{o,n}" based on nn_idx and euclidean distance
					vector<double> dists_2d(k+1, 0.0);
					for (int j = 0; j <= k; ++j) {
						double d = 0.0, d_aux;
						d_aux = cloud->points[i].x - cloud->points[nn_idx[j]].x;
						d += d_aux * d_aux;
						d_aux = cloud->points[i].y - cloud->points[nn_idx[j]].y;
						d += d_aux * d_aux;
						dists_2d[j] = sqrt(d);
					}
					// Below, dists_nd will ultimately store the difference between the two vectors
					double norm_nd = sqrt(inner_product(dists_nd, dists_nd + k + 1, dists_nd, 0.0L));
					double norm_2d = sqrt(inner_product(dists_2d.begin(), dists_2d.begin() + k + 1, dists_2d.begin(), 0.0L));
					for (int j = 0; j <= k; ++j) {
						dists_nd[j] /= norm_nd;
						dists_2d[j] /= norm_2d;
						dists_nd[j] -= dists_2d[j];
					}
					// The "pps" vector is the "true_neighborhood"
					_values[i] = (float)sqrt(inner_product(dists_nd, dists_nd + k + 1, dists_nd, 0.0L));
				}
				max_error = std::max(max_error, (double)_values[i]);
			}
			cout << "True Neighbors -- Max. error: " << max_error << endl;
			//if (diss_type > 0) {
			//	// normalize
			//	float max_val = 0.0f;
			//	for (int i = 0; i < N; ++i)
			//		max_val = std::max(max_val, _values[i]);
			//	for (int i = 0; i < N; ++i)
			//		_values[i] /= max_val;
			//}

		}
    return;
  }
	else {
		const std::vector<ANNidxArray>& matrix = nd ? cloud->neighborhood_matrix_nd : cloud->neighborhood_matrix_2d;

		for (int i = 0; i < N; ++i)
			_values[i] = 0.0f;

		for (int i = 0; i < N; ++i) {
			if (selected_pid == -1 || i == selected_pid) {
				for (int j = 0; j <= k; ++j) {
					if (nearest_first)
						_values[matrix[i][j]] += (float) k - j + 1;
					else
						_values[matrix[i][j]] += (float) j;
				}
			}
		}

		if (excl && selected_pid > -1) {
			// use the other matrix
			const std::vector<ANNidxArray>& other_matrix = nd ? cloud->neighborhood_matrix_2d : cloud->neighborhood_matrix_nd;
			// and set all to 0.0f, so we show exclusively values from the originally selected matrix
			for (int j = 0; j <= k; ++j)
				_values[other_matrix[selected_pid][j]] = 0.0f;
		}
	}
}

void Neighborhood::compute(float norm, int pid) {
	selected_pid = pid;

	computeValues(_values, cloud, tex_nd, tex_k, tex_nearest_first, tex_excl);

	normalize(norm);

	if (draw_edges) {
		computeValues(edge_values, cloud, edg_nd, tex_k, tex_nearest_first, edg_excl);
		computeEdges();
	}

	updateImage();
}

static void control_cb(int ctrl) {
	_instance->compute(0.0f, selected_pid);
	/*
	switch (ctrl) {
		case UI_NEIGH_KNN_TEX:
		case UI_NEIGH_TEX_D:
			//if ((neigh_tex_d != neigh_edg_d) || (knn_tex != knn_edg))
			_instance->compute(knn_tex, neigh_tex_d, selected_point_id, neigh_nearest_first_colors, neigh_tex_excl);
			// Synchronize K
			if (neigh_sync_k && knn_tex != knn_edg) {
			neigh_knn_edg_scr->set_int_val(knn_tex);
			neigh_knn_edg_scr->do_callbacks();
			}
			break;
		case UI_NEIGH_KNN_EDG:
		case UI_NEIGH_EDG_D:
			//if ((neigh_tex_d != neigh_edg_d) || (knn_tex != knn_edg))
			_instance->compute(knn_edg, neigh_edg_d, selected_point_id, neigh_nearest_first_colors, neigh_edg_excl);
			computeNeighborhoodGraph();
			computeBundles();
			// Synchronize K
			if (neigh_sync_k && knn_tex != knn_edg) {
				neigh_knn_tex_scr->set_int_val(knn_edg);
				neigh_knn_tex_scr->do_callbacks();
			}
			break;
		case UI_NEIGH_nearest_first_COLORS:
		case UI_NEIGH_TEX_EXCL:
		case UI_NEIGH_EDG_EXCL:
			_instance->compute(knn_tex, neigh_tex_d, selected_point_id, neigh_nearest_first_colors, neigh_tex_excl);
			//computeNeighborhoodGraph();
			//computeBundles();
			break;
	}
	*/
	//_glui->sync_live();
	_glui->post_update_main_gfx();						//Update GLUT window upon any parameter change (i.e., redraw)
}


void Neighborhood::options(GLUI* glui) {
	_instance = this;
	_glui = glui;
	GLUI_Panel *pan, *pan2;
	// 5.     Neighborhood Settings Panel
	GLUI_Rollout* ui_neighborhood = glui->add_rollout("Neighborhood", false);
	// 5.1    Texture Settings Panel
	pan = glui->add_panel_to_panel(ui_neighborhood, "Texture");
	//        Exclusive or not
	pan2 = glui->add_panel_to_panel(pan, "", GLUI_PANEL_NONE);
	new GLUI_Checkbox(pan2, "Exclusive", &tex_excl, UI_NEIGH_TEX_EXCL, control_cb);
	glui->add_column_to_panel(pan2, false);
	new GLUI_Checkbox(pan2, "Nearest first", &tex_nearest_first, UI_NEIGH_nearest_first_COLORS, control_cb);
	// 5.1.1  Radio: show 2D or nD k-neighborhood
	pan2 = glui->add_panel_to_panel(pan, "", GLUI_PANEL_NONE);
	GLUI_RadioGroup *ui_neigh_tex_d = new GLUI_RadioGroup(pan2, &tex_nd, UI_NEIGH_TEX_D, control_cb);
	new GLUI_RadioButton(ui_neigh_tex_d, "2D");
	if (cloud->dimensions() > 0) {
		new GLUI_RadioButton(ui_neigh_tex_d, "nD");
		new GLUI_RadioButton(ui_neigh_tex_d, "True");
	}
	// 5.1.2  Scrollbar: k (size of k-neighborhood)
	glui->add_column_to_panel(pan2, false);	
	GLUI_Spinner *neigh_knn_tex_scr = new GLUI_Spinner(pan2, "KNN: ", GLUI_SPINNER_INT, &tex_k, UI_NEIGH_KNN_TEX, control_cb);
	neigh_knn_tex_scr->set_int_limits(1, cloud->size());
	pan2 = glui->add_panel_to_panel(pan2, "", GLUI_PANEL_NONE);
	GLUI_Listbox* diss = new GLUI_Listbox(pan2, "Diss.: ", &diss_type, UI_DISS, control_cb);
  diss->add_item(0, "Jaccard");
	diss->add_item(1, "Custom / K+1");
	diss->add_item(2, "Custom / True Rank");
	diss->add_item(3, "pps (Schreck 2010)");
	// 5.2    Edges Settings Panel
	pan = glui->add_panel_to_panel(ui_neighborhood, "Edges");
	// 5.2.1  Checkbox: Enable/Disable edges
	pan2 = glui->add_panel_to_panel(pan, "", GLUI_PANEL_NONE);
	new GLUI_Checkbox(pan2, "Enable", &draw_edges, UI_NEIGH_BUNDLES, control_cb);	
	//        Exclusive or not
	glui->add_column_to_panel(pan2, false);
	new GLUI_Checkbox(pan2, "Exclusive", &edg_excl, UI_NEIGH_EDG_EXCL, control_cb);	
	// 5.2.2  Radio: show 2D or nD neighborhood
	pan2 = glui->add_panel_to_panel(pan, "", GLUI_PANEL_NONE);
	GLUI_RadioGroup *ui_neigh_edg_d = new GLUI_RadioGroup(pan2, &edg_nd, UI_NEIGH_EDG_D, control_cb);
	new GLUI_RadioButton(ui_neigh_edg_d, "2D");
	if (cloud->dimensions() > 0) {
		new GLUI_RadioButton(ui_neigh_edg_d, "nD");
		new GLUI_RadioButton(ui_neigh_edg_d, "True");
	}
	/*
	// 5.2.3  Scrollbar: k (size of k-neighborhood)
	glui->add_column_to_panel(pan2, false);
	new GLUI_StaticText(pan2, "KNN:");
	neigh_knn_edg_scr = new GLUI_Spinner(pan2, "", GLUI_SPINNER_INT, &knn_edg, UI_NEIGH_KNN_EDG, control_cb);
	neigh_knn_edg_scr->set_int_limits(1, cloud->size());
	// 5.3    Checkbox: Synchronize K
	pan = glui->add_panel_to_panel(ui_neighborhood, "", GLUI_PANEL_NONE);
	new GLUI_Checkbox(pan, "Synchronized K", &neigh_sync_k, UI_NEIGH_SYNC_K, control_cb);
	*/
	
	
	// End of 5
}
