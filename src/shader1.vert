#version 440

uniform float rad_blur;
uniform float rad_max;

in vec2 vert;
in float scalar;

out float value;

void main() {
  // Maybe I should check the maximum point radius and output something to the user
  gl_PointSize = (rad_max + rad_blur) * 2.0;
  gl_Position = vec4((vert/512.0)*2.0-1.0, 0, 1); // quebra-galho
  value = scalar;
}
