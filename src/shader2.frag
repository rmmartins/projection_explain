#version 440

layout (location = 0) out vec4 fragColor;

uniform sampler2D siteDT;
uniform sampler2D accumTex;
uniform sampler1D colormap;
uniform float rad_max;

vec3 getRGB (float value) {
  // "Rainbow" colormap is currently hard-coded. This should be a colormap lookup.
  /*
  const float dx = 0.8;
  float val = (6.0-2.0*dx)*value+dx;
  float R = max(0.0,(3.0-abs(val-4.0)-abs(val-5.0))/2.0);
  float G = max(0.0,(4.0-abs(val-2.0)-abs(val-4.0))/2.0);
  float B = max(0.0,(3.0-abs(val-1.0)-abs(val-2.0))/2.0);
  return vec3(R, G, B);
  */
  return texture(colormap, mix(0.005f, 0.995f, value)).rgb;
}

void main() {
  float dt = texelFetch(siteDT, ivec2(gl_FragCoord.xy), 0).r;
  if (dt > rad_max)
    discard;
  else {
    vec4 accum = texelFetch(accumTex, ivec2(gl_FragCoord.xy), 0);
    // 1.0 is extra-accumulated because of white background
    float value = 0.0f;
    if (accum.g > 1.0)
      value = (accum.r - 1.0) / (accum.g - 1.0);
    fragColor.rgb = getRGB(value);
    fragColor.a = 1.0-dt/rad_max;
  }
}
