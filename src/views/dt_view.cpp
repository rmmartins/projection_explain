#include "view.h"

void DTView::updateImage() {

  //This is the area-of-influence of a point in _2D_ (thus, image-size-fixed)
  const float radius = cloud->avgdist;

  //0. Create 'tex_dt':
  float dt_max = cloud->DT_max;
  for (int i = 0; i < size*size; ++i)													// Generate visualization texture
  {
    float dt  = cloud->siteDT[i];
    float r,g,b;
    projwiz::float2rgb(dt/dt_max,r,g,b,true);
    _out_image[i * 4 + 0] = r;
    _out_image[i * 4 + 1] = g;
    _out_image[i * 4 + 2] = b;
    _out_image[i * 4 + 3] = 1.0f;
  }

  updateTexture();

}