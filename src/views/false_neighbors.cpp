#ifdef PLATFORM_WIN
#define NOMINMAX
#include <algorithm>
#endif

#include "view.h"

void FalseNeighbors::updateImage (float range, float distweight) {
  const float radius = cloud->avgdist;
  
  float* mask = new float[size * size * 2];
  //0. Create 'tex_mask' and 'tex_dt':
  for (int i = 0; i < size*size; ++i)                         // Generate visualization texture
  {
    float dt  = cloud->siteDT[i];
    float rdt = dt;
    if (rdt>radius) rdt=radius;
    rdt = 1-pow(rdt/radius,1.0f);

    mask[2*i]   = 1;
    mask[2*i+1] = rdt;
  }

  _image->interpolateDistMatrix(*cloud,distweight);               //Reconstruct the false-positive image from the core data
  
  //1. Create 'tex_false_neighbors': Holds the interpolated image
  float norm = (range)? range : _image->image_max;				//Normalize 'image' automatically or vs user-specified range
  float r,g,b;
  int id;
  float val;
  for (int i = 0; i < size; ++i)															//Generate visualization texture
    for (int j = 0; j < size; ++j)
    {
      id   = j * size + i;
      val  = std::min(_image->image->value(i,j)/norm,1.0f);
      float cert = _image->certainty->value(i,j);
      float   dt = mask[2*id+1];
      cert = cert * dt;

      projwiz::float2rgb(val,r,g,b,true); // color_mode is deprecated

      float lum = 1;
      r = (1-cert)*lum + cert*r;
      g = (1-cert)*lum + cert*g;
      b = (1-cert)*lum + cert*b;

      _out_image[id * 4 + 0] = r;
      _out_image[id * 4 + 1] = g;
      _out_image[id * 4 + 2] = b;
      _out_image[id * 4 + 3] = 1.0f;
    }

    delete[] mask;

    updateTexture();

}