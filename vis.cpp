#include "include/glwrapper.h"

#include "include/vis.h"
#include "include/scalarimage.h"
#include "include/pointcloud.h"
#include "include/graph.h"
#include "include/fullmatrix.h"
#include "include/orderedmatrix.h"
#include "include/sortederrmatrix.h"
#include "include/io.h"
#include "include/grouping.h"
#include "include/vgrouping.h"
#include "include/skelft.h"
#include "include/gdrawingcloud.h"
#include "include/cpubundling.h"

#ifdef PLATFORM_OSX
#include <cutil_inline.h>								// includes cuda.h and cuda_runtime_api.h
#include <cutil_gl_inline.h>
#else // Linux, Windows
#include <cuda.h>
#include <cuda_runtime_api.h>
#endif

#include <iostream>
#include <vector>
#include <map>
#include <math.h>
#include "include/config.h"
#include <FreeImage.h>
#include <set>
#include <algorithm>

using namespace std;


extern RadialGrouping* visual_clustering;				//!!To be added to some clean interface
extern Grouping*	   labelg;


enum
{
  UI_SCALE_DOWN=1,
  UI_SCALE_UP,
  UI_TEX_INTERP,
  UI_SHOW_POINTS,
  UI_SHOW_DELAUNAY,
  UI_COLOR_MODE,
  UI_SHOW_GROUPS,
  UI_SHOW_MAPTYPE,
  UI_GROUP_FINER,
  UI_GROUP_COARSER,
  UI_SHOW_BRUSH,
  UI_SHOW_SELECTION,
  UI_SHOW_CLOSEST_SEL,
  UI_RECOMPUTE_CUSHIONS,
  UI_SKEL_CUSHIONS,
  UI_CUSHION_THICKNESS,
  UI_CUSHION_ALPHA,
  UI_MAP_ALPHA,
  UI_POINTS_ALPHA,
  UI_FALSENEIGH_DISTWEIGHT,
  UI_FALSENEIGH_RANGE,
  UI_MISSNEIGH_RANGE,
  UI_AGGREGATE_ERR_RANGE,
  UI_CUSHION_COLORING,
  UI_CUSHION_STYLE,
  UI_POINT_RADIUS,
  UI_SHEPARD_AVERAGING,
  UI_CUSHION_OPENING,
  UI_SHOW_BUNDLES,
  UI_BUNDLE_ITERATIONS,
  UI_BUNDLE_KERNEL,
  UI_BUNDLE_EDGERES,
  UI_BUNDLE_SMOOTH,
  UI_BUNDLE_DENS_ESTIM,
  UI_BUNDLE_CPU_GPU,
  UI_BUNDLE_SAVE,
  UI_BUNDLE_FALSE_POS,
  UI_QUIT,
  UI_SCREENSHOT,
  UI_ERROR_TYPE,
  UI_NORM_ERROR,
  UI_COLORMAP,
};

// Global settings
int show_maptype = 0;

static int colormap = 0;
static int norm_error = 1;
static int		 show_delaunay  = 0;
static int		 color_mode     = 1;
static int		 show_groups    = 0;
static int		 show_bundles	= 0;
static int neigh_bundles = 0;
static int		 show_selection = 1;
static int		 show_closest_sel = 1;
static float	 map_alpha		= 1;
static int		 skeleton_cushions = 0;
static float	 cushion_threshold = 0.35f;
static float	 cushion_shading_thickness = 30;
static float	 point_size     = 2;
static float	 cushion_alpha  = 1;
static int		 cushion_coloring = 1;
static int		 cushion_style  = 0;
static float	 opening_threshold = 20;
static float	 frac_miss_neighbors_bundle = 0.01f;	//Fraction of missing neighbor edges (from total N^2 edges) to show in bundling
static int knn_tex = 0;
static int knn_edg = 0;
static int		 density_estimation = 0;
static int neigh_edg_d = 0;
static int neigh_sync_k = true;
static int neigh_tex_excl = false;
static int neigh_edg_excl = false;
static int		 gpu_bundling = 1;
static GLUI_StaticText* ui_miss_neighbors_bundling_size = 0;
static GLUI_StaticText* ui_selection_size = 0;
static float	 MAX_SELECT_DIST = 20;					//Max distance for interactively selecting points (screen-space)
static float false_neighbor_distweight = 10.0f;
static float false_neighbor_range = 0;
static int			knn = 10;
static float missing_neighbor_range = 0;


static int error_type = 0;


static vector<hash_set<int> >							//For each group, the triangle-ids in the Delaunay triangulation of the cloud
xtris;									//whose vertices belong to this group


Display*		Display::instance = 0;					//!!!Move following vars to class...
VisualGrouping	visual_groups;
float*			skel_dt;
float*			dt_param;
CPUBundling*	bund = 0;								//Bundling engine (for various graphs)
Graph*			miss_neighbors_graph = 0;				//Graph linking a point with its missing neighbors, for all points
GraphDrawingCloud*
miss_neighbors_bundling = 0;			//Drawing of above

Grouping::PointGroup selection;							//Selected points in the visualization
set<int>			selected_group;					//Id of selected label-group (if any was selected), else -1


static int colormap_width = 128;
static int colormap_height = 8;
GLuint Display::tex_colormap;

// Scalars and Views

AggregateError* aggregate_error;
MissingNeighbors* missing_neighbors;
LabelMixing* label_mixing;
FalseNeighbors* false_neighbors;
DTView* dt_view;
Neighborhood* neighborhood;
DimRanking* dim_ranking;


//------------------------------------------------------------------------------------------------


void draw_image (Display& dpy, int tex_name) {
  glEnable(GL_TEXTURE_2D);
  setTexture(tex_name, dpy.tex_interp);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glColor4f(1,1,1,map_alpha);
  glBegin(GL_QUADS);
  float size_f = (float)dpy.winSize;
  glTexCoord2f(0.0f, 0.0f); glVertex2f(0.0f, 0.0f);
  glTexCoord2f(1.0f, 0.0f); glVertex2f(size_f, 0.0f);
  glTexCoord2f(1.0f, 1.0f); glVertex2f(size_f, size_f);
  glTexCoord2f(0.0f, 1.0f); glVertex2f(0.0f, size_f);
  glEnd();
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);

  glEnable(GL_TEXTURE_1D);
  glBindTexture(GL_TEXTURE_1D, Display::tex_colormap);
  /*
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (tex_interp)?GL_LINEAR:GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (tex_interp)?GL_LINEAR:GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  */
  glBegin(GL_QUADS);
  glTexCoord2f(0.0f, 0.0f); glVertex2f(10.0f, 10.0f);
  glTexCoord2f(1.0f, 0.0f); glVertex2f((float)colormap_width, 10.0f);
  glTexCoord2f(1.0f, 1.0f); glVertex2f((float)colormap_width, 10.0f + colormap_height);
  glTexCoord2f(0.0f, 1.0f); glVertex2f(10.0f, 10.0f + colormap_height);
  glEnd();
  glDisable(GL_TEXTURE_1D);
  glColor4f(0,0,0,1);
  glBegin(GL_LINE_STRIP);
  glVertex2i(10, 10);
  glVertex2i(colormap_width, 10);
  glVertex2i(colormap_width, 10 + colormap_height);
  glVertex2i(10, 10 + colormap_height);
  glVertex2i(9, 9); // ???
  glEnd();

}


void draw_selection(Display& dpy)
{
  if (show_selection)
  {
    glEnable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for(int i=0;i<2;++i)											//1. Draw selected points:
    {
      float size  = (2-i)*point_size*dpy.scale;
      glPointSize(size);
      if (i==0)
        glColor3f(0,0,1);
      else
        glColor3f(1,1,1);
      glBegin(GL_POINTS);
      for(Grouping::PointGroup::const_iterator it=selection.begin();it!=selection.end();++it)
      {
        int i = *it;
        const Point2d& p = dpy.cloud->points[i];
        glVertex2f(p);
      }
      glEnd();
    }
    glDisable(GL_BLEND);
    glPointSize(1);
  }

  if (show_closest_sel)
  {
    if (dpy.selected_point_id!=-1)									//2. Draw closest-selected-point:
    {
      glShadeModel(GL_FLAT);
      for(int i=0;i<2;++i)										//Draw a nice outlined cursor at 'selected_point_id'
      {
        float color = (i==0)?1.0f:0.0f;
        float width = (i==0)?3.0f:1.0f;
        glColor3f(color,color,color);							//Draw a crosshairs at selected point
        glLineWidth(width);
        const Point2d& sp = dpy.cloud->points[dpy.selected_point_id];
        float del = 0.025f*dpy.winSize;
        glBegin(GL_LINES);
        glVertex2f(sp.x-del,sp.y);
        glVertex2f(sp.x+del,sp.y);
        glVertex2f(sp.x,sp.y-del);
        glVertex2f(sp.x,sp.y+del);
        glEnd();
      }
      glLineWidth(1);
    }
  }
}


void draw_missing_neighbors(Display& dpy)
{
  if (selection.size()==0)
    if (dpy.selected_point_id == -1)
      return;								//No selected points? Nothing to do
    else
    {
      selection.insert(selected_point_id);
      missing_neighbors->compute(missing_neighbor_range, selection);	//4. Update all visualizations that depend on 'selection'
      dpy.updateViews();
      dpy.computeMissingNeighborsGraph();
      dpy.computeBundles();
      dpy.glui->sync_live();											//Force updating the GL window
    }
  draw_image(dpy,dpy.tex_missing_neighbors);
}


Display::Display(int winSize_,						//Graphics window size; can be anything smaller, equal, or bigger to texture size
    PointCloud* cloud_,				//Point cloud
    int argc,char** argv):				//Arguments: needed for GLUT
  imgSize(winSize_),					//WARNING: Here, we assume the image is square and pow(2)
  cloud(cloud_),
  winSize(winSize_),
  scale(1),
  transX(0),transY(0),
  isLeftMouseActive(false),isRightMouseActive(false),
  oldMouseX(0),oldMouseY(0),
  tex_interp(true),
  closest_point(-1),
  selected_point_id(-1)
{

  instance = this;

  cudaMallocHost((void**)&cushion_param,winSize*winSize*sizeof(float));
  cudaMallocHost((void**)&cushion_dt,winSize*winSize*sizeof(float));
  splat_img = new float[winSize*winSize];
  dt_param  = new float[winSize*winSize];
  skel_dt   = new float[winSize*winSize];

  glutInitWindowSize(winSize, winSize);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_ALPHA);
  glutInit(&argc, argv);
  glutWin = glutCreateWindow("Point cloud interpolation");

  GLenum err = glewInit();
  if (GLEW_OK != err)
  {
	  /* Problem: glewInit failed, something is seriously wrong. */
	  fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	  exit(-1);
  }
  fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

  glutDisplayFunc(display_cb);
  glutMouseFunc(mouse_cb);
  glutKeyboardFunc(keyboard_cb);
  glutMotionFunc(motion_cb);
  glutPassiveMotionFunc(passivemotion_cb);

  // Textures for displaying various objects
  GLuint texture[7];
  glGenTextures(7, texture);   // Generate all required textures
  tex_sites = texture[0];
  tex_splat = texture[1];
  tex_point_density = texture[2];
  tex_framebuffer_lum = texture[3];
  tex_density = texture[4];
  tex_framebuffer_rgba = texture[5];
  tex_colormap = texture[6];


  GLuint framebuffers[2];
  glGenFramebuffersEXT(2,framebuffers);   // Make two offscreen framebuffers: high-res luminance one (for accurate splatting) and low-res RGBA one (for all other ops)

  framebuffer_lum  = framebuffers[0];
  framebuffer_rgba = framebuffers[1];

  glBindFramebuffer(GL_FRAMEBUFFER,framebuffer_lum);
  glBindTexture(GL_TEXTURE_2D,tex_framebuffer_lum);
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA32F,winSize,winSize,0,GL_LUMINANCE,GL_UNSIGNED_BYTE,0);		//Make empty texture
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,GL_COLOR_ATTACHMENT0_EXT,GL_TEXTURE_2D,tex_framebuffer_lum,0);	//Connect texture to framebuffer

  glBindFramebuffer(GL_FRAMEBUFFER,framebuffer_rgba);
  glBindTexture(GL_TEXTURE_2D,tex_framebuffer_rgba);
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,winSize,winSize,0,GL_RGBA,GL_UNSIGNED_BYTE,0);							//Make empty texture
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,GL_COLOR_ATTACHMENT0_EXT,GL_TEXTURE_2D,tex_framebuffer_rgba,0);//Connect texture to framebuffer

  glBindFramebuffer(GL_FRAMEBUFFER,0);		//Make sure we draw in the onscreen buffer, after all above stuff

  visual_groups.init(labelg);
  visual_groups.cushion_shading_thickness = cushion_shading_thickness;

  bund = new CPUBundling(winSize);				//Create bundling engine; we'll use it for several graph bundling tasks in this class
  bund->initEdgeProfile(CPUBundling::PROFILE_HOURGLASS);
  bund->verbose = false;
  density_estimation = bund->density_estimation;

  glPixelStorei(GL_UNPACK_ALIGNMENT,1);			//Initialize some state-related stuff in this
  glPixelStorei(GL_PACK_ALIGNMENT,1);
  makeSplat(512,2.0);								//Create a reasonably fine-grained radial distance splat
  makeDensitySplat(512);							//REMARK: Obsolete?

  drawColormap();

  // Scalars and Views:
  aggregate_error = new AggregateError(cloud);
  aggregate_error->compute(aggregate_error_range);
  missing_neighbors = new MissingNeighbors(cloud);
  label_mixing = new LabelMixing(cloud);
  label_mixing->compute();
  neighborhood = new Neighborhood(cloud);
  false_neighbors = new FalseNeighbors(cloud);
  dt_view = new DTView(cloud);
  dim_ranking = new DimRanking(cloud);

  views.push_back(false_neighbors);
  views.push_back(missing_neighbors);
  views.push_back(aggregate_error);
  views.push_back(label_mixing);
  views.push_back(dt_view);
  views.push_back(neighborhood);
  views.push_back(dim_ranking);

  computeTriangleMap();							//Requires a RGBA framebuffer
  computeMissingNeighborsGraph();
  //updateViews();
  computeAllCushions();
  computeGroupMeshes(visual_clustering);
  updateViews();

  knn_edg = cloud->size() / 10;
  //computeNeighborhoodGraph();

  computeBundles();

  char buf[128];
  GLUI_Panel *pan,*pan2;								//Construct GUI:
  GLUI_Scrollbar* scr;
  GLUI_Spinner* spn;
  glui = GLUI_Master.create_glui("Projections Wizard");

  GLUI_Rollout* ui_map = glui->add_rollout("Map",false);					//1. Panel "Map":
  pan = glui->add_panel_to_panel(ui_map,"Show what");
  GLUI_Listbox* ui_maptype = new GLUI_Listbox(pan, "Map type: ", &show_maptype, UI_SHOW_MAPTYPE, control_cb);
  for (unsigned int i = 0; i < views.size(); ++i) {
    ui_maptype->add_item(i, views[i]->name.c_str());
  }
  pan = glui->add_panel_to_panel(ui_map,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"False neighbors smoothing: ");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&false_neighbor_distweight,UI_FALSENEIGH_DISTWEIGHT,control_cb);
  scr->set_int_limits(0,50);
  pan = glui->add_panel_to_panel(ui_map,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"False neighbors range: ");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&false_neighbor_range,UI_FALSENEIGH_RANGE,control_cb);
  scr->set_float_limits(0,1);
  pan = glui->add_panel_to_panel(ui_map,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Missing neighbors range");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&missing_neighbor_range,UI_MISSNEIGH_RANGE,control_cb);
  scr->set_float_limits(0,1);
  pan = glui->add_panel_to_panel(ui_map,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Aggregate error range");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&aggregate_error_range,UI_AGGREGATE_ERR_RANGE,control_cb);
  scr->set_float_limits(0,1);

  pan = glui->add_panel_to_panel(ui_map,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Map alpha");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&map_alpha,UI_MAP_ALPHA,control_cb);
  scr->set_float_limits(0,1);


  GLUI_Rollout* ui_visuals = glui->add_rollout("Point cloud",false);	//2. Panel "Visual settings":
  pan = glui->add_panel_to_panel(ui_visuals,"Cloud triangulation");
  GLUI_RadioGroup* ui_delaunay = new GLUI_RadioGroup(pan,&show_delaunay,UI_SHOW_DELAUNAY,control_cb);
  new GLUI_RadioButton(ui_delaunay,"Don't show");
  new GLUI_RadioButton(ui_delaunay,"Black");
  new GLUI_RadioButton(ui_delaunay,"Colored");

  pan = glui->add_panel_to_panel(ui_visuals,"Cloud drawing");
  GLUI_Listbox* ui_lb = new GLUI_Listbox(pan,"Drawing",&show_particles,UI_SHOW_POINTS,control_cb);
  ui_lb->add_item(0,"Nothing");
  ui_lb->add_item(1,"Black");
  ui_lb->add_item(2,"Color by label");
  for(unsigned int i=0;i<cloud->attributes.size();++i)
  {
    char buf[120];
    sprintf(buf,"Attribute %d",i);
    ui_lb->add_item(i+3,buf);
  }


  pan = glui->add_panel_to_panel(ui_visuals,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Points alpha");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&points_alpha,UI_POINTS_ALPHA,control_cb);
  scr->set_float_limits(0,1);

  GLUI_Rollout* ui_grps = glui->add_rollout("Point groups",false);		//3. Panel "Point groups":
  pan = glui->add_panel_to_panel(ui_grps,"Display groups");
  GLUI_RadioGroup* ui_groups = new GLUI_RadioGroup(pan,&show_groups,UI_SHOW_GROUPS,control_cb);
  new GLUI_RadioButton(ui_groups,"Nothing");
  new GLUI_RadioButton(ui_groups,"Visual groups");
  new GLUI_RadioButton(ui_groups,"Label groups");
  new GLUI_RadioButton(ui_groups,"Mesh-based groups");
  new GLUI_RadioButton(ui_groups,"Point density");

  pan = glui->add_panel_to_panel(ui_grps,"Cushion style");
  GLUI_RadioGroup* ui_cushion_style = new GLUI_RadioGroup(pan,&cushion_style,UI_CUSHION_STYLE,control_cb);
  new GLUI_RadioButton(ui_cushion_style,"Border");
  new GLUI_RadioButton(ui_cushion_style,"Full");

  new GLUI_Checkbox(ui_grps,"Skeleton cushions", &skeleton_cushions, UI_RECOMPUTE_CUSHIONS, control_cb);
  new GLUI_Checkbox(ui_grps,"Cushion coloring", &cushion_coloring, UI_CUSHION_COLORING, control_cb);
  pan = glui->add_panel_to_panel(ui_grps,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Cushion border");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&cushion_shading_thickness,UI_CUSHION_THICKNESS,control_cb);
  scr->set_float_limits(1,50);
  pan = glui->add_panel_to_panel(ui_grps,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Cushion thickness");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&cushion_threshold,UI_RECOMPUTE_CUSHIONS,control_cb);
  scr->set_float_limits(0,1);
  pan = glui->add_panel_to_panel(ui_grps,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Cushion smoothness");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&opening_threshold,UI_CUSHION_OPENING,control_cb);
  scr->set_float_limits(1,100);
  pan = glui->add_panel_to_panel(ui_grps,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Cushion alpha");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&cushion_alpha,UI_CUSHION_ALPHA,control_cb);
  scr->set_float_limits(0,1);

  new GLUI_Button(ui_grps,"Recompute cushions",UI_RECOMPUTE_CUSHIONS,control_cb);

  GLUI_Rollout* ui_stats = glui->add_rollout("Statistics",false);			//4. Panel "Statistics":
  pan = glui->add_panel_to_panel(ui_stats,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Points");
  glui->add_column_to_panel(pan,false);
  sprintf(buf,"%d",cloud->size());
  new GLUI_StaticText(pan,buf);
  pan = glui->add_panel_to_panel(ui_stats,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Dimensions");
  glui->add_column_to_panel(pan,false);
  sprintf(buf,"%d",cloud->dimensions());
  new GLUI_StaticText(pan,buf);
  pan = glui->add_panel_to_panel(ui_stats,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Labels");
  glui->add_column_to_panel(pan,false);
  sprintf(buf,"%d",int(cloud->numLabels()));
  new GLUI_StaticText(pan,buf);
  pan = glui->add_panel_to_panel(ui_stats,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"False-positive bundles");
  glui->add_column_to_panel(pan,false);
  sprintf(buf,"%d",miss_neighbors_bundling->numEdges());
  ui_miss_neighbors_bundling_size = new GLUI_StaticText(pan,buf);
  pan = glui->add_panel_to_panel(ui_stats,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Selected points");
  glui->add_column_to_panel(pan,false);
  sprintf(buf, "%d", (int)selection.size());
  ui_selection_size = new GLUI_StaticText(pan,buf);


  //!!!Show data for selected-point

  GLUI_Rollout* ui_settings = glui->add_rollout("General settings",false);//5. Panel "Settings":
  new GLUI_Checkbox(ui_settings, "Interpolate tex", &tex_interp, UI_TEX_INTERP, control_cb);
  new GLUI_Checkbox(ui_settings, "Colormapping", &color_mode, UI_COLOR_MODE, control_cb);
  new GLUI_Checkbox(ui_settings, "Brush", &show_brush, UI_SHOW_BRUSH, control_cb);
  new GLUI_Checkbox(ui_settings, "Selection", &show_selection, UI_SHOW_SELECTION, control_cb);
  new GLUI_Checkbox(ui_settings, "Closest selected point", &show_closest_sel, UI_SHOW_CLOSEST_SEL, control_cb);
  pan = glui->add_panel_to_panel(ui_settings,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Point drawing radius");
  glui->add_column_to_panel(pan,false);
//  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&cloud->avgdist,UI_POINT_RADIUS,control_cb);
  spn = glui->add_spinner_to_panel(pan,"",GLUI_SPINNER_FLOAT,&cloud->avgdist,UI_POINT_RADIUS,control_cb);
  spn->set_float_limits(2,75);
  pan = glui->add_panel_to_panel(ui_settings,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Shepard smoothing");
  glui->add_column_to_panel(pan,false);
  glui->add_spinner_to_panel(pan,"",GLUI_SPINNER_FLOAT,&shepard_averaging,UI_SHEPARD_AVERAGING,control_cb);
  spn->set_int_limits(1,50);

  new GLUI_Button(glui,"Quit",UI_QUIT,control_cb);						//5. "Quit" button:


  glui->add_column(true);													//--------------------------------------------------------

  GLUI_Rollout* ui_bundling = glui->add_rollout("Bundling",false);		//4. Panel "Bundling":
  pan = glui->add_panel_to_panel(ui_bundling,"Missing neighbors (all)");
  new GLUI_StaticText(pan,"Show what");
  GLUI_RadioGroup* ui_bundles = new GLUI_RadioGroup(pan,&show_bundles,UI_SHOW_BUNDLES,control_cb);
  new GLUI_RadioButton(ui_bundles,"Nothing");
  new GLUI_RadioButton(ui_bundles,"Bundles");

  pan = glui->add_panel_to_panel(ui_bundling,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Missing neighbors to show");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&frac_miss_neighbors_bundle,UI_BUNDLE_FALSE_POS,control_cb);
  scr->set_float_limits(0,1);

  pan2 = glui->add_panel_to_panel(ui_bundling,"General options");
  pan = glui->add_panel_to_panel(pan2,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Iterations");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->niter,UI_BUNDLE_ITERATIONS,control_cb);
  scr->set_int_limits(0,30);
  pan = glui->add_panel_to_panel(pan2,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Kernel size");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->h,UI_BUNDLE_KERNEL,control_cb);
  scr->set_float_limits(3,40);
  pan = glui->add_panel_to_panel(pan2,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Smoothing");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->lambda,UI_BUNDLE_SMOOTH,control_cb);
  scr->set_float_limits(0,1);
  pan = glui->add_panel_to_panel(pan2,"",GLUI_PANEL_NONE);
  new GLUI_StaticText(pan,"Edge sampling");
  glui->add_column_to_panel(pan,false);
  scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->spl,UI_BUNDLE_EDGERES,control_cb);
  scr->set_float_limits(3,50);
  pan = glui->add_panel_to_panel(pan2,"Density estimation");
  GLUI_RadioGroup* ui_dens_estim = new GLUI_RadioGroup(pan,&density_estimation,UI_BUNDLE_DENS_ESTIM,control_cb);
  new GLUI_RadioButton(ui_dens_estim,"Exact");
  new GLUI_RadioButton(ui_dens_estim,"Fast");
  new GLUI_Checkbox(pan2,"GPU method", &gpu_bundling,UI_BUNDLE_CPU_GPU,control_cb);
  new GLUI_Button(ui_bundling,"Save",UI_BUNDLE_SAVE,control_cb);

  for (auto it = views.begin(); it != views.end(); ++it) {
    (*it)->options(glui);
  }

  // 6.   Error Type Panel
//  pan = glui->add_panel("Error Type");
//  GLUI_RadioGroup *ui_error_type = new GLUI_RadioGroup(pan, &error_type, UI_ERROR_TYPE, control_cb);
//  new GLUI_RadioButton(ui_error_type, "Distance");
//  new GLUI_RadioButton(ui_error_type, "Neighborhood");
  // End of 6

  // 7.   Colormap chooser
  pan = glui->add_panel("", GLUI_PANEL_NONE);
  GLUI_Listbox *colormap_listbox = glui->add_listbox_to_panel(pan, "Colormap: ", &colormap, UI_COLORMAP, control_cb);
  vector<string> colormaps = getColormaps();
  for (unsigned int i = 0; i < colormaps.size(); ++i)
    colormap_listbox->add_item(i, colormaps[i].c_str());
  // End of 7

  new GLUI_Button(glui,"Screenshot",UI_SCREENSHOT,control_cb);

  glui->set_main_gfx_window(glutWin);										//Link GLUI with GLUT (seems needed)
}


Display::~Display()															//Dtor
{																			//!!Not ready: much more needs to be deleted
  delete miss_neighbors_bundling;
  delete miss_neighbors_graph;
  delete bund;
  delete dt_param;
  delete skel_dt;
  cudaFreeHost(cushion_param);
  cudaFreeHost(cushion_dt);
  delete[] splat_img;
  // Scalars and Views:
  delete aggregate_error;
  delete missing_neighbors;
  delete label_mixing;
  delete neighborhood;
  delete false_neighbors;
  delete dim_ranking;
}


void Display::computeMissingNeighborsGraph()	//Build graph of most important 'frac_miss_neighbors_bundle'-percent of missing neighbors
{
  int NP = cloud->size(), Nmax;

  typedef multimap<float,Graph::Edge> ValueEdges;
  ValueEdges sorted;
  const PointCloud::DistMatrix& dm = *cloud->distmatrix;  //1. Sort all MNs (so we can next select most important ones):
  if (selection.size()) //1.1. If we have a selection, we only show edges going to/from selected points:
  {
    for(Grouping::PointGroup::const_iterator it=selection.begin();it!=selection.end();++it)
    { //Start with one endpoint IN selection:
      int i = *it;
      const PointCloud::DistMatrix::Row& row = dm(i);
      for(int j=0;j<NP;++j)
      {
        float err = row[j];
        if (err<0) continue;  //Skip false neighbors, we don't want those ones
        if (selection.find(j)!=selection.end()) continue;	//Skip edges ENDING in selection (since we want only edges linking selection with outside)
        sorted.insert(make_pair(err,make_pair(i,j)));
      }
    }
    Nmax = sorted.size();
  }
  else for(int i=0;i<NP;++i)  //1.2. Nothing selected: show all MNs in entire dataset:
  {
    const PointCloud::DistMatrix::Row& row = dm(i);
    for(int j=i+1;j<NP;++j)
    {
      float err = row[j];
      if (err<0) continue;  //Skip false neighbors, we don't want those ones
      sorted.insert(make_pair(err,make_pair(i,j)));
    }
    Nmax = int(frac_miss_neighbors_bundle * sorted.size());  //Retain only the most important 'frac_miss_neighbors_bundle' MNs
  }

  //2. Construct the graph (as a sparse adj-matrix)
  int I = 0;
  float max_err, min_err;
  delete miss_neighbors_graph;
  miss_neighbors_graph = new Graph(NP);
  for(ValueEdges::const_reverse_iterator it=sorted.rbegin();it!=sorted.rend() && I<Nmax;++it,++I)
  {
    if (it == sorted.rbegin()) max_err = it->first;
    min_err = it->first;
    const Graph::Edge& edge = it->second;
    (*miss_neighbors_graph)(edge.first,edge.second) = min_err;
    (*miss_neighbors_graph)(edge.second,edge.first) = min_err;
  }

  delete miss_neighbors_bundling;
  miss_neighbors_bundling = new GraphDrawingCloud();  //3. Construct a graph drawing for 'miss_neighbors_graph'
  miss_neighbors_bundling->build(miss_neighbors_graph,cloud);

  if (ui_miss_neighbors_bundling_size)  //3. Update Statistics UI (if any)
  {
    char buf[128];
    sprintf(buf,"%d",miss_neighbors_bundling->numEdges());
    ui_miss_neighbors_bundling_size->set_text(buf);
  }
}


void Display::computeGroupMeshes(Grouping* g)
{
  xtris.resize(g->size());

  for(int i=0;i<g->size();++i)
  {
    hash_set<int>& tris = xtris[i];
    tris.clear();

    Grouping::PointGroup pg;
    g->group(i,pg);

    for(Grouping::PointGroup::const_iterator it=pg.begin();it!=pg.end();++it)
    {
      int pid = *it;										//Get the triangle-fan (fine-scale) of fine-scale point

      const PointCloud::TrisOfPoint& fan = cloud->point2tris[pid];
      for(PointCloud::TrisOfPoint::const_iterator fti=fan.begin();fti!=fan.end();++fti)
      {													//Select all triangles in fan with vertices only in 'pg'
        int ft = *fti;
        const Triangle& tr = cloud->triangles[ft];
        if (pg.find(tr(0))==pg.end()) continue;
        if (pg.find(tr(1))==pg.end()) continue;
        if (pg.find(tr(2))==pg.end()) continue;
        tris.insert(ft);
      }
    }
  }
}


void Display::drawMap() // Display one of the various dense maps:
{
  draw_image(*this, views[show_maptype]->tex());
}


void Display::drawGroups()
{
  if (show_groups==3)											//Show mesh-based groups
  {
    drawGroupMeshes();
    return;
  }

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

  if (show_groups==2)											//Show image-based label groups:
  {
    for(int i=0;i<labelg->size();++i)
    {
      if (selected_group.find(i) == selected_group.end()) continue;
      VisualGrouping::Cushion* c = visual_groups.getCushion(i);
      visual_groups.draw(c,cushion_alpha,cushion_coloring,tex_interp);
    }
  }

  if (show_groups==1)											//Show visual grouping of all points in the cloud:
  {
    VisualGrouping::Cushion* c = visual_groups.getCushion(-1);
    visual_groups.draw(c,cushion_alpha,false,tex_interp);
  }

  if (show_groups==4)											//Show the density map of all points in the cloud:
  {
    glColor4f(0,0,0,cushion_alpha);
    setTexture(tex_point_density,tex_interp);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f,0.0f); glVertex2f(0.0f, 0.0f);
    glTexCoord2f(1.0f,0.0f); glVertex2f((float)winSize, 0.0f);
    glTexCoord2f(1.0f,1.0f); glVertex2f((float)winSize, (float)winSize);
    glTexCoord2f(0.0f,1.0f); glVertex2f(0.0f, (float)winSize);
    glEnd();
  }

  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
}



void Display::startOffscreen(int ncomponents)
{
  if (ncomponents==1)
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,framebuffer_lum);		//Draw in the offscreen buffer:
  else
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,framebuffer_rgba);

  glClearColor(0,0,0,0);
  glClear(GL_COLOR_BUFFER_BIT);									//Start clean

  glViewport(0,0,winSize,winSize);
  glMatrixMode(GL_PROJECTION);									//Setup projection matrix
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0,winSize,0,winSize);
  glMatrixMode(GL_MODELVIEW);										//Must reset viewing transformations, since we next
  glPushMatrix();													//want to draw all points as they are in the cloud (i.e., untransformed)
  glLoadIdentity();
}

void Display::endOffscreen()
{
  glMatrixMode(GL_PROJECTION);									//Restore whatever transformations we had originally
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,0);						//Redirect drawing in the onscreen buffer
}


void Display::computeTriangleMap()
{
  startOffscreen(4);
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
  glShadeModel(GL_FLAT);

  glBegin(GL_TRIANGLES);											//Render all triangles, and encode 1+triangle-id in the rendered RGB color.
  for(int i=0,NT=cloud->triangles.size();i<NT;++i)				//This creates a color buffer where 0 means no triangle, and else we can
  {																//find the triangle-id from the color
    unsigned int id = i+1;
    unsigned int i0 = id & 255; id >>= 8;
    unsigned int i1 = id & 255; id >>= 8;
    unsigned int i2 = id & 255;
    glColor3ub(i2,i1,i0);

    const Triangle& tr = cloud->triangles[i];
    glVertex2f(cloud->points[tr(0)]);
    glVertex2f(cloud->points[tr(1)]);
    glVertex2f(cloud->points[tr(2)]);
  }
  glEnd();

  glReadPixels(0,0,winSize,winSize,GL_RGBA,GL_UNSIGNED_BYTE,cloud->buff_triangle_id);
  unsigned char* buff = (unsigned char*)cloud->buff_triangle_id;	//We read the data as RGBA (for speed). Postprocess it so that in each
  for(int i=0;i<winSize*winSize;++i)								//buff_triangle_id[] element we truly have the triangle-id+1 at that pixel, or 0.
  {
    unsigned int v2 = *buff++;
    unsigned int v1 = *buff++;
    unsigned int v0 = *buff++;
    buff++;
    cloud->buff_triangle_id[i] = (v2<<16) + (v1<<8) + v0;
  }

  endOffscreen();
}




void Display::drawGroupMeshes()
{
  int NG = xtris.size();
  for(int i=0;i<NG;++i)
  {
    float r,g,b;
    projwiz::float2rgb(float(i)/NG,r,g,b,color_mode);
    glColor3f(r,g,b);

    glBegin(GL_TRIANGLES);
    hash_set<int>& tris = xtris[i];
    for(hash_set<int>::const_iterator it=tris.begin();it!=tris.end();++it)
    {
      int tid = *it;
      const Triangle& t = cloud->triangles[tid];
      glVertex2f(cloud->points[t(0)]);
      glVertex2f(cloud->points[t(1)]);
      glVertex2f(cloud->points[t(2)]);
    }
    glEnd();

  }
}


void Display::displayCb()
{
  glClearColor(1,1,1,1);											//Reset main GL state to defaults
  glClear(GL_COLOR_BUFFER_BIT);
  glDisable(GL_LIGHTING);
  glDisable(GL_DEPTH_TEST);

  glViewport(0,0,winSize,winSize);
  glMatrixMode(GL_PROJECTION);									//Setup projection matrix
  glLoadIdentity();
  gluOrtho2D(0,winSize,0,winSize);
  glMatrixMode(GL_MODELVIEW);										//Setup modelview matrix
  glLoadIdentity();
  glScalef(scale,scale,1);
  glTranslatef(transX,transY,0);


  //1. Draw the current map-visualization on the background
  drawMap();

  //2. Draw the groups atop of this visualization, appropriately blended
  if (show_groups!=0) drawGroups();

  //3. Draw the Delaunay edges
  if (show_delaunay!=0) drawDelaunay();

  //4. Draw the points in the cloud
  drawPoints();

  //5. Draw any bundles that may be there
  drawBundles();

  //6. Draw any selection that may be there
  draw_selection(*this);

  //7. Draw the interactive brush
  drawBrush();

  glutSwapBuffers();												// All done
}



void Display::drawDelaunay()
{
  glColor3f(0,0,0);

  glBegin(GL_LINES);
  for(unsigned int i=0;i<cloud->points.size();++i)
  {
    const Point2d& pi = cloud->points[i];
    const PointCloud::EdgeMatrix::Row& row = (*cloud->sorted_edges)(i);
    for(PointCloud::EdgeMatrix::Row::const_iterator it = row.begin();it!=row.end();++it)
    {
      int     j = it->pid;
      const Point2d& pj = cloud->points[j];

      if (show_delaunay==2)
      {
        float r,g,b;
        float v = (*cloud->distmatrix)(i,j);
        projwiz::float2rgb(v,r,g,b,color_mode);
        glColor3f(r,g,b);
      }

      glVertex2f(pi);
      glVertex2f(pj);
    }
  }
  glEnd();
}

void Display::drawBundles()
{
  switch (show_bundles)
  {
    case 0:
			if (neighborhood->draw_edges && selected_point_id > -1 && neighborhood->bundling)
        neighborhood->bundling->draw(1.0f);
      break;
    case 1:
      miss_neighbors_bundling->draw(missing_neighbor_range);
      break;
  }
}

void Display::drawPoints()											//Draw points in the cloud
{
  if (show_particles==0) return;									//No point drawing

  glEnable(GL_POINT_SMOOTH);
  glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glPointSize(point_size*scale);
  glColor4f(cloud_color[0],cloud_color[1],cloud_color[2],points_alpha);

  const vector<float>* attrs=0; float sm,rng;

  if (show_particles==2)
  {
    sm    = cloud->class_data_min;
    rng   = cloud->class_data_max - sm;
    attrs = &cloud->class_data;
  }
  else if (show_particles>2)
  {
    int a = show_particles-3;
    sm    = cloud->attributes_min[a];
    rng   = cloud->attributes_max[a]-sm;
    attrs = cloud->attributes[a];
  }

  if (rng<1.0e-6) rng = 1;

  glBegin(GL_POINTS);
  for(unsigned int i=0;i<cloud->points.size();++i)
  {
    const Point2d& p = cloud->points[i];
    if (attrs)
    {
      float r,g,b;
      float v = ((*attrs)[i]-sm)/rng;
      projwiz::float2rgb(v,r,g,b,color_mode);
      glColor4f(r,g,b,points_alpha);
    }
    glVertex2f(p);
  }
  glEnd();

  glDisable(GL_BLEND);
  glPointSize(1);
}


void Display::makeSplat(int SZ,float sigma)							//Create a Gaussian splat texture SZ*SZ pixels. Used later for density-map construction
{
  float* img = new float[SZ*SZ];

  const float half = 0.5;
  const float C = (float)SZ/2.0f;
  const float C2 = C*C;											//Generate a half-sphere height profile, encode it as a luminance-alpha texture
  const float  k = exp(-sigma);
  for(int i=0,idx=0;i<SZ;++i)										//The height should be normalized in [0..1]; alpha indicates the valid vs nonvalid pixels
    for(int j=0;j<SZ;++j,++idx)
    {
      float x  = i-C+half, y = j-C+half;
      int   r2 = int(x*x+y*y);
      if (r2>C2)												//Point outside the ball: color irrelevant, and it's transparent
      {
        img[idx] = 0;
      }
      else
      {
        float   D = (exp(-sigma*r2/C2)-k)/(1-k);
        img[idx] = D;
      }
    }

  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D,tex_splat);
  glTexImage2D(GL_TEXTURE_2D,0,GL_LUMINANCE,SZ,SZ,0,GL_LUMINANCE,GL_FLOAT,img);
  glDisable(GL_TEXTURE_2D);
  delete[] img;
}


void Display::makeDensitySplat(int SZ)								//
{
  float* img = new float[SZ*SZ];

  const float half = 0.5;
  const float C = (float)SZ/2.0f;
  const float C2 = C*C;											//Generate a half-sphere height profile, encode it as a luminance-alpha texture
  for(int i=0,idx=0;i<SZ;++i)										//The height should be normalized in [0..1]; alpha indicates the valid vs nonvalid pixels
    for(int j=0;j<SZ;++j,++idx)
    {
      float x  = i-C+half, y = j-C+half;
      int   r2 = int(x*x+y*y);
      if (r2>C2)												//Point outside the ball: color irrelevant, and it's transparent
      {
        img[idx] = 0;
      }
      else
      {
        float   D = 1-(r2/C2)*(r2/C2);
        img[idx] = D;
      }
    }

  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D,tex_density);
  glTexImage2D(GL_TEXTURE_2D,0,GL_LUMINANCE32F_ARB,SZ,SZ,0,GL_LUMINANCE,GL_FLOAT,img);
  glDisable(GL_TEXTURE_2D);
  delete[] img;
}



void Display::computeBundles()
{
  bool anything_to_draw = false;
  if (neighborhood->draw_edges && neighborhood->bundling && neighborhood->bundling->numEdges() > 0) {
    anything_to_draw = true;
    bund->setInput(neighborhood->bundling);
  } else if (miss_neighbors_bundling->numEdges() > 0) {
    anything_to_draw = true;
    bund->setInput(miss_neighbors_bundling); //Bundle the graph drawing, and save the bundling
  }
  if (anything_to_draw)
    if (gpu_bundling)
      bund->bundleGPU();
    else
      bund->bundleCPU();
}



void Display::computeAllCushions()
{
  float* cushions = new float[winSize*winSize];				//1. Compute the visual cushions and cloud's density map
  bool norm = computeCushions(0,cushions);
  visual_groups.setCushion(-1,cushions,norm);

  glBindTexture(GL_TEXTURE_2D,tex_point_density);				//Save the density map (for visual debugging purposes)
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,winSize,winSize,0,GL_ALPHA,GL_FLOAT,splat_img);

  const int NG = labelg->size();								//2. Compute cushions for all groups:
  for(int i=0;i<NG;++i)										//
  {															//
    Grouping::PointGroup pg;									//
    labelg->group(i,pg);
    bool norm = computeCushions(&pg,cushions);
    visual_groups.setCushion(i,cushions,norm);
  }

  delete[] cushions;

  visual_groups.makeTextures();								//Make all textures for the existing visual cushions
}




int Display::computeCushions(Grouping::PointGroup* pg,float* output)
{
  startOffscreen(4);

  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE,GL_ONE);										//We want to accumulate (add) what we draw
  setTexture(tex_splat,true);
  glColor3f(0.1f,0.1f,0.1f);											//Splat must be drawn grey, given that we next read/threshold luminance

  const int winSize2 = winSize*winSize;

  glBegin(GL_QUADS);
  if (!pg)														//Splat the entire cloud:
    for(int i=0,N=cloud->size();i<N;++i)							//1. Compute the fuzzy distance map:
    {
      const Point2d& p = cloud->points[i];
      float rad = cloud->avgdist;									//Radius: What we'd need, is an estimation of the LOCAL inter-point distance..
      if (rad<10) rad=10;											//We don't want too small radii - visually, we cannot distinguish differences at such resolutions
      drawSplat(p,rad);
    }
  else															//Splat a single group:
  {
    memset(dt_param,0,winSize2*sizeof(float));					//Need to compute DT of only points in _given_ group:
    for(Grouping::PointGroup::const_iterator it=pg->begin();it!=pg->end();++it)
    {
      int i = *it;
      const Point2d& p = cloud->points[i];
      float rad = cloud->avgdist;									//Radius: What we'd need, is an estimation of the LOCAL inter-point distance..
      if (rad<10) rad=10;											//We don't want too small radii - visually, we cannot distinguish differences at such resolutions
      drawSplat(p,rad);
      dt_param[int(p.y)*winSize+int(p.x)] = 1;
    }
  }
  glEnd();

  glDisable(GL_TEXTURE_2D);
  glDisable(GL_BLEND);											//Get the distance map in 'splat_img'
  glReadPixels(0,0,winSize,winSize,GL_LUMINANCE,GL_FLOAT,splat_img);

  float* dt;														//dt = DT of point-set to use (either whole point-cloud or group)
  /*
     if (!pg)
     dt = cloud->siteDT;
     else															//For groups, we must compute their point-set DT here on the fly:
     {
     skelft2DFT(0,dt_param,0,0,winSize,winSize,winSize);			//Compute FT of the sites
     skelft2DDT(dt_param,0,0,winSize,winSize);					//Compute DT of the sites (from the resident FT)
     dt = dt_param;
     }
     */

  //!!!
  dt = splat_img;

  skelft2DMakeBoundary(dt,0,0,winSize,winSize,cushion_param,winSize,cushion_threshold,true);
  skelft2DFT(0,cushion_param,0,0,winSize,winSize,winSize);		//1. Threshold 'dt' to obtain a contour roughly around all points
  skelft2DDT(cushion_dt,0,0,winSize,winSize);						//1.1. Compute DT of this contour (for thinning)

  for(int i=0;i<winSize2;++i)										//1.1. Erode the contour with a distance 'opening_threshold' inwards
    dt_param[i] = (cushion_dt[i]>opening_threshold &&
        dt[i]>cushion_threshold);						//This removes thin features of the contour (which is good)

  skelft2DFT(0,dt_param,0,0,winSize,winSize,winSize);				//2. Compute DT of the eroded contour (for inflation)
  skelft2DDT(cushion_dt,0,0,winSize,winSize);						//

  for(int i=0;i<winSize2;++i)										//3. Inflate the eroded contour outwards with a distance 'opening_threshold'
    if (cushion_dt[i]>opening_threshold)							//For this, compute the eroded contour's DT and threshold it by 'opening_threshold'
      cushion_dt[i]=0;											//The result is a kind of morphological opening of the initial contour
    else
      cushion_dt[i] = opening_threshold - cushion_dt[i];

  float length = skelft2DMakeBoundary(cushion_dt,0,0,winSize,winSize,cushion_param,winSize,0.00001f,true);
  skelft2DFT(0,cushion_param,0,0,winSize,winSize,winSize);		//4. Finally, compute the inwards-DT of the thinned-and-inflated shape
  skelft2DDT(skel_dt,0,0,winSize,winSize);						//This is simply needed for shading the interior of this shape
  for(int i=0;i<winSize2;++i)
    cushion_dt[i] = (cushion_dt[i]>0)? skel_dt[i] : 0;

  if (skeleton_cushions)											//Use skeleton-cushion-shading: compute interpolation of shape-to-skeleton
  {																//(via well-known blend formula)
    skelft2DSkeleton(0,length,30,0,0,winSize,winSize);
    skel2DSkeletonDT(skel_dt,0,0,winSize,winSize);
  }

  for(int i=0;i<winSize2;++i)
  {
    float v=-1;													//Outside cushions: set height to -1 (marker for outside)
    if (cushion_dt[i]>0)											//Inside cushions: copy height from either DT or skeleton-DT interpolation
    {
      if (skeleton_cushions)										//Skeleton cushions:
        v = skel_dt[i];
      else														//Classical DT cushions:
      {
        v = cushion_dt[i]-1;										//Small correction of DT needed, since 'inside' means DT>0..
        if (v<0) v=0;
      }
    }
    output[i] = v;												//output[] is always in {-1} U [0,1]
  }

  endOffscreen();
  return skeleton_cushions;										//Skeleton cushions are normalized; DT-cushions are not
}


void Display::updateViews()
{
  //glEnable(GL_TEXTURE_2D);

  for (unsigned int i = 0; i < views.size(); ++i) {
    views[i]->updateImage();
  }

  //glDisable(GL_TEXTURE_2D);
}


std::vector<int> *isect_all, *diffN_all, *diff2_all;
ANNpointArray	dataNd, data2d;
ANNkd_tree *treeNd, *tree2d;

void Display::initKNNSearchStructures() {
	int nd = cloud->attributes.size(), np = cloud->size();

	dataNd = annAllocPts(np, nd);			// allocate data points
  data2d = annAllocPts(np, 2);			// allocate data points

	// load data
  for (int i = 0; i < np; i++) {
    for (int j = 0; j < nd; j++) {
      dataNd[i][j] = (*(cloud->attributes[j]))[i];
		}
    data2d[i][0] = cloud->points[i].x;
    data2d[i][1] = cloud->points[i].y;
  }

  treeNd = new ANNkd_tree(					// build search structure
      dataNd,					// the data points
      np,						// number of points
      nd);						// dimension of space

  tree2d = new ANNkd_tree(					// build search structure
      data2d,					// the data points
      np,						// number of points
      2);						// dimension of space

}

// this has to be called when "knn" changes
void Display::calculateNeighborhood() {

  int nd = cloud->attributes.size(), np = cloud->size();

	isect_all = new std::vector<int>[np];
	diffN_all = new std::vector<int>[np];
	diff2_all = new std::vector<int>[np];

  ANNidxArray nnIdxNd, nnIdx2d;					// near neighbor indices
  ANNdistArray distsNd, dists2d;					// near neighbor distances


  nnIdxNd = new ANNidx[knn];						// allocate near neigh indices
  distsNd = new ANNdist[knn];						// allocate near neighbor dists
  nnIdx2d = new ANNidx[knn];						// allocate near neigh indices
  dists2d = new ANNdist[knn];						// allocate near neighbor dists

  ANNpoint queryPtNd, queryPt2d;

  // visit every point and calculate (i) error, (ii) missing neighbors and (iii) false neighbors

  for (int i = 0; i < np; i++) {
    queryPtNd = dataNd[i];
    queryPt2d = data2d[i];

    treeNd->annkSearch(						// search
        queryPtNd,						// query point
        knn,								// number of near neighbors
        nnIdxNd,							// nearest neighbors (returned)
        distsNd,							// distance (returned)
        0.0);							// error bound

    std::sort (nnIdxNd, nnIdxNd+knn);

    tree2d->annkSearch(						// search
        queryPt2d,						// query point
        knn,								// number of near neighbors
        nnIdx2d,							// nearest neighbors (returned)
        dists2d,							// distance (returned)
        0.0);							// error bound

    std::sort (nnIdx2d, nnIdx2d+knn);

    std::vector<int> isect(knn), diffN(knn), diff2(knn);
    std::vector<int>::iterator it;

    it=std::set_intersection (nnIdxNd, nnIdxNd+knn, nnIdx2d, nnIdx2d+knn, isect.begin());
    isect.resize(it-isect.begin());
    it=std::set_difference (nnIdxNd, nnIdxNd+knn, nnIdx2d, nnIdx2d+knn, diffN.begin());
    diffN.resize(it-diffN.begin());
    it=std::set_difference (nnIdx2d, nnIdx2d+knn, nnIdxNd, nnIdxNd+knn, diff2.begin());
    diff2.resize(it-diff2.begin());

		isect_all[i] = isect;
		diffN_all[i] = diffN;
		diff2_all[i] = diff2;
  }

  delete [] nnIdxNd;							// clean things up
  delete [] nnIdx2d;							// clean things up
  delete [] distsNd;
  delete [] dists2d;
  annClose();									// done with ANN

	cout << "KNN: " << knn << endl;

}


void Display::mouseCb(int button, int state, int x, int y)
{
  if (state == GLUT_UP)												//Mouse button release:
    switch (button)
    {
      case GLUT_LEFT_BUTTON:
        isLeftMouseActive = false;
        break;
      case GLUT_RIGHT_BUTTON:
        isRightMouseActive = false;
        break;
    }

  int modif = glutGetModifiers();

  if (state == GLUT_DOWN)												//Mouse button click:
  {
    oldMouseX = x;
    oldMouseY = y;

    switch (button)
    {
      case GLUT_LEFT_BUTTON:											//Left button click:
        {
          isLeftMouseActive = true;
          float xf = (float(x))/scale - transX;
          float yf = (winSize-float(y))/scale - transY;

          vector<int> nn;												//Find closest cloud-point to mouse
          vector<float> nd;
          Point2d pix(xf,yf);
          cloud->searchNN(pix,1,nn,&nd);
          if (!(modif & GLUT_ACTIVE_SHIFT)) selection.clear();		//Clear selection, if not in additive mode

          if (nd[0]>MAX_SELECT_DIST*MAX_SELECT_DIST)					//Clicked too far from any point: no selection
          {
            selected_group.clear();
            selected_point_id = -1;
          }
          else														//Clicked close enough to a point: select something:
          {
            selected_point_id = nn[0];								//Remember closest point to mouse
            cout << "selected_point_id = " << selected_point_id << endl;

            if (modif & GLUT_ACTIVE_CTRL)							//CTRL-click: select all points in group under mouse
            {
              int gid = visual_groups.cushionAtPoint(pix);			//Select all points in the label-group under the mouse
              if (gid!=-1)
              {														//Add selected points to current-selection
                Grouping::PointGroup sel = visual_groups.groupAtPoint(gid);
                for(Grouping::PointGroup::const_iterator it=sel.begin();it!=sel.end();++it)
                  selection.insert(*it);
                selected_group.insert(gid);
              }
            }
            else													//Normal click: add closest point to mouse to selection
              selection.insert(selected_point_id);
          }

          if (ui_selection_size)										//3. Update Statistics UI (if any)
          {
            char buf[128];
            sprintf(buf, "%d", (int)selection.size());
            ui_selection_size->set_text(buf);
          }

          missing_neighbors->compute(missing_neighbor_range, selection);	//4. Update all visualizations that depend on 'selection'
          updateViews();
          computeMissingNeighborsGraph();
          neighborhood->compute(0.0, selected_point_id);
          //if (neigh_tex_d != neigh_edg_d)
            //neighborhood->compute(knn_edg, neigh_edg_d, selected_point_id, neigh_inv_colors, neigh_edg_excl);
          //computeNeighborhoodGraph();
          computeBundles();

          glui->sync_live();											//Force updating the GL window
          break;
        }
      case GLUT_RIGHT_BUTTON:											//Right button click:
        isRightMouseActive = true;
        break;
    }
  }

  glutPostRedisplay();
}





void Display::drawBrush()												//Interactive data brushing. Closest point in cloud is already stored in 'closest_point'
{
  if (closest_point==-1 || !show_brush) return;

  const Point2d& closest = cloud->points[closest_point];

  glPointSize(5);														//1. Draw closest point
  glColor3f(1,0,0);
  glBegin(GL_POINTS);
  glVertex2f(closest);
  glEnd();
  glPointSize(1);

  const float rad = cloud->avgdist;
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
  glColor4f(0.0f, 0.0f, 0.0f, 0.4f);
  drawCircle(closest,rad);
  glDisable(GL_BLEND);

  int tri = cloud->hitTriangle(brush_point);
  if (tri>=0)
  {
    const Triangle& tr = cloud->triangles[tri];
    glColor3f(1,1,1);													//2. Draw Delaunay triangle containing mouse cursor
    glLineWidth(3);														//   (if any was found..)
    const Point2d& p0 = cloud->points[tr(0)];
    const Point2d& p1 = cloud->points[tr(1)];
    const Point2d& p2 = cloud->points[tr(2)];
    glBegin(GL_LINES);
    glVertex2f(p0);
    glVertex2f(p1);
    glVertex2f(p0);
    glVertex2f(p2);
    glVertex2f(p1);
    glVertex2f(p2);
    glEnd();
    glLineWidth(1);
  }

  const PointCloud::EdgeMatrix::Row& row = (*cloud->sorted_edges)(closest_point);
  glColor3f(0,0,0);
  for(PointCloud::EdgeMatrix::Row::const_iterator it = row.begin();it!=row.end();++it)
  {
    int   j = it->pid;
    const Point2d& pj = cloud->points[j];

    glBegin(GL_LINES);
    glVertex2f(closest);
    glVertex2f(pj);
    glEnd();

    glRasterPos2f(pj.x,pj.y);

    char buf[100];
    sprintf(buf,"%d",j);
    glutDrawString(buf);
  }
}



void Display::passivemotionCb(int x,int y)
{
  if (x<0 || y<0 || x>winSize-1 || y>winSize-1) return;	//Don't track mouse if outside window

  brush_point = Point2d((float)x, (float)winSize-y);
  brush_point.x /= scale;									//Transform back from pixel coordinates to world coordinates
  brush_point.y /= scale;
  brush_point.x -= transX;
  brush_point.y -= transY;

  vector<int> res;										//Find cloud point closest to the mouse position
  cloud->searchNN(brush_point,1,res);
  closest_point = res[0];									//Remember that point for drawing the brush later

  glutPostRedisplay();
}



void Display::motionCb(int x,int y)
{
  if (x<0 || y<0 || x>winSize-1 || y>winSize-1) return;	//Don't track mouse if outside window

  if (isLeftMouseActive)
  {
    transX += float(x - oldMouseX) / scale;
    transY -= float(y - oldMouseY) / scale;
    glutPostRedisplay();
  }
  else if (isRightMouseActive)
  {
    scale -= (y - oldMouseY) * scale / 400.0f;
    glutPostRedisplay();
  }

  oldMouseX = x; oldMouseY = y;
}

BYTE* pixels;
FIBITMAP* fi_image;

void Display::controlCb(int ctrl)
{
  switch(ctrl)
  {
    case UI_SCREENSHOT:
      pixels = (BYTE*)malloc((3 * imgSize * imgSize)*sizeof(BYTE));
      // Make the BYTE array, factor of 3 because it's RBG.
      glReadPixels(0, 0, imgSize, imgSize, GL_BGR, GL_UNSIGNED_BYTE, pixels);
      // Convert to FreeImage format & save to file
      fi_image = FreeImage_ConvertFromRawBits(pixels, imgSize, imgSize, 3 * imgSize, 24, 0x0000FF, 0xFF0000, 0x00FF00, false);
      FreeImage_Save(FIF_PNG, fi_image, "screenshot.png", 0);
      // Free resources
      FreeImage_Unload(fi_image);
      delete [] pixels;
      break;
    case UI_QUIT:
      exit(0);
      break;
    case UI_SCALE_DOWN:
      scale *= 0.9f;
      break;
    case UI_SCALE_UP:
      scale *= 1.1f;
      break;
    case UI_COLOR_MODE:
      updateViews();
      break;
    case UI_CUSHION_STYLE:
      visual_groups.cushion_type = VisualGrouping::CUSHION_TYPE(cushion_style);
      visual_groups.makeTextures();
      break;
    case UI_CUSHION_THICKNESS:
      visual_groups.cushion_shading_thickness = cushion_shading_thickness;
      visual_groups.makeTextures();
      break;
    case UI_FALSENEIGH_DISTWEIGHT:
    case UI_FALSENEIGH_RANGE:
      false_neighbors->updateImage(false_neighbor_range, false_neighbor_distweight);
      break;
    case UI_MISSNEIGH_RANGE:
      missing_neighbors->compute(missing_neighbor_range, selection);
      break;
    case UI_AGGREGATE_ERR_RANGE:
      aggregate_error->compute(aggregate_error_range);
      break;
    case UI_SHEPARD_AVERAGING:
      ImageInterpolator::rad_blur = shepard_averaging;
      // fall through
    case UI_POINT_RADIUS:
      views[show_maptype]->updateImage();
      break;
    case UI_CUSHION_OPENING:
      computeAllCushions();
      break;
    case UI_GROUP_FINER:								//Refine cloud: if refinement exists, use it.
      {													//If not, we're already at the finest level, so nowhere to refine
        if (visual_clustering->finer)
        {
          visual_clustering = visual_clustering->finer;
          computeGroupMeshes(visual_clustering);
        }
        break;
      }
    case UI_GROUP_COARSER:								//Coarsen cloud: use existing coarser-cloud, or coarsen on demand
      {
        if (!visual_clustering->coarser)
          visual_clustering = visual_clustering->coarsen();
        else visual_clustering = visual_clustering->coarser;
        computeGroupMeshes(visual_clustering);
        break;
      }
    case UI_RECOMPUTE_CUSHIONS:
      computeAllCushions();
      break;
    case UI_BUNDLE_ITERATIONS:
    case UI_BUNDLE_KERNEL:
    case UI_BUNDLE_EDGERES:
    case UI_BUNDLE_SMOOTH:
    case UI_BUNDLE_CPU_GPU:
      computeBundles();
      break;
    case UI_BUNDLE_DENS_ESTIM:
      bund->density_estimation = (CPUBundling::DENSITY_ESTIM)density_estimation;
      computeBundles();
      break;
    case UI_BUNDLE_FALSE_POS:
      computeMissingNeighborsGraph();
      computeBundles();
      break;
    case UI_BUNDLE_SAVE:
      miss_neighbors_bundling->saveTrails("miss_neighbors.trl");
      break;

    case UI_ERROR_TYPE:
    /*
      if (error_type) {
//        cloud->distmatrix_dist = cloud->distmatrix;
        cloud->distmatrix = neighborhood->distmatrix();
      } else {
//        cloud->distmatrix_neigh = cloud->distmatrix;
        cloud->distmatrix = cloud->distmatrix_dist;
      }
      aggregate_error->compute(aggregate_error_range);
      missing_neighbors->compute(missing_neighbor_range, selection);
      computeMissingNeighborsGraph();
      computeBundles();
      updateViews();
      updateViews();
      */
      break;
    case UI_COLORMAP:
      checkLoadColormap(colormap);
      drawColormap();
      views[show_maptype]->updateImage();
      break;
    case UI_SHOW_MAPTYPE:
      views[show_maptype]->updateImage();
      break;
  }

  glui->post_update_main_gfx();						//Update GLUT window upon any parameter change (i.e., redraw)
}


void Display::keyboardCb(unsigned char k,int,int)
{
  switch (k)
  {
    case 'q':  controlCb(UI_QUIT);				break;
    case '.':  controlCb(UI_SCALE_DOWN);		break;
    case ',':  controlCb(UI_SCALE_UP);			break;
    case 't':  tex_interp = !tex_interp;		break;
    case 'p':  show_particles = (show_particles+1)%3;
               break;
    case 'c':  color_mode = !color_mode;
               updateViews();				break;
    case 'b':  show_brush = !show_brush;		break;
    case ' ':  show_maptype = (show_maptype+1)%6;
               break;
    case '-': case '_':
               controlCb(UI_GROUP_FINER);		break;
    case '+': case '=':
               controlCb(UI_GROUP_COARSER);		break;
    case 'g':  show_groups = (show_groups+1)%5; break;
  }

  glui->sync_live();
  glui->post_update_main_gfx();
}


void Display::keyboard_cb(unsigned char k,int x,int y)
{
  instance->keyboardCb(k,x,y);
}

void Display::control_cb(int ctrl)									//Static entry point for UI events; calls non-static method
{
  instance->controlCb(ctrl);
}

void Display::display_cb()											//Driver callback for GLUT
{
  instance->displayCb();
}

void Display::mouse_cb(int button, int state, int x, int y)
{
  instance->mouseCb(button,state,x,y);
}

void Display::motion_cb(int x,int y)
{
  instance->motionCb(x,y);
}

void Display::passivemotion_cb(int x,int y)
{
  instance->passivemotionCb(x,y);
}


void Display::drawColormap() {
  GLfloat *data = new GLfloat[colormap_width*3];
  for (int i = 0; i < colormap_width; ++i) {
    projwiz::float2rgb((float)i/(float)colormap_width, data[3*i], data[3*i+1], data[3*i+2], true);
//    std::cout << data[3*i] << " " << data[3*i+1] << " " << data[3*i+2] << std::endl;
  }
  glBindTexture(GL_TEXTURE_1D, tex_colormap);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, colormap_width, 0, GL_RGB, GL_FLOAT, data);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  delete data;
}
